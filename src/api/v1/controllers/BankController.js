import { 
    list_bank_service,
    list_byid_service,
    create_bank_service,
    update_bank_service,
    delete_bank_service
} from "../services/Bank.js";
import authJWT from "../middleware/authJWT.js";

/**
 * BANK CONTROLLER
 * ==============================
 * Created by Harival Tivani
 * Created at December 30, 2021
 * ==============================
 */

export const list_bank = async (req, res) => {
    return res.status(200).json(await list_bank_service());
}

export const create_bank = async (req, res) => {
    const user_id = await authJWT.authData(req, res);
    return res.status(200).json(await create_bank_service(req.body, user_id));
}

export const update_bank = async (req, res) => {
    const user_id = await authJWT.authData(req, res);
    return res.json(await update_bank_service(req.body, req.params.id, user_id));
}

export const delete_bank = async (req, res) => {
    const user_id = await authJWT.authData(req, res);
    return res.json(await delete_bank_service(req.body));
}

export const get_byid = async (req, res) => {
    return res.json(await list_byid_service(req.params.id));
}