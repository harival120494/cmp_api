import { 
    marketplace_list_service,
    marketplace_create_service,
    marketplace_update_service,
    marketplace_byid_service,
    marketplace_delete_service,
    marketplace_list_byuser_service
} from "../services/Marketplace.js";
import authJWT from "../middleware/authJWT.js";
import { validationResult } from "express-validator";

/**
 * PACKAGE CONTROLLER
 * ==============================
 * Created by Harival Tivani
 * Created at December 27, 2021
 * ==============================
 */


export const marketplace_list = async (req, res) => {
    return res.json(await marketplace_list_service());
}

export const marketplace_create = async (req, res) => {
    const user_id = await authJWT.authData(req, res);
    const errors = validationResult(req).errors;
    return res.json(await marketplace_create_service(req.body, user_id));
}

export const marketplace_update = async (req, res) => {
    const user_id = await authJWT.authData(req, res);
    const errors = validationResult(req).errors;
    return res.json(await marketplace_update_service(req.body, req.params.id, user_id));
    // if (errors.length > 0) {
    //     return res.status(400).json({error: errors})
    // }
    // else{
        
    //     return res.json(await marketplace_update_service(req.body, req.params.id, user_id));
    // }
}

export const marketplace_byid = async (req, res) => {
    return res.json(await marketplace_byid_service(req.params.id));
}

export const marketplace_delete = async (req, res) => {
    const user_id = await authJWT.authData(req, res);
    return res.json(await marketplace_delete_service(req.body.marketplace_id, req.body.status, user_id));
}

export const marketplace_list_byuser = async (req, res) => {
    let BASE_URL = req.protocol+"://"+req.headers.host+'/';
    const user_id = await authJWT.authData(req, res);
    return res.json(await marketplace_list_byuser_service(user_id, BASE_URL));
}