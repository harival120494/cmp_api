import { 
    create_order_paket_service,
    pembayaran_service,
    upload_bukti_pembayaran_service,
    list_pembayaran_service,
    detail_list_pembayaran_service,
    accept_pembayaran_service
} from "../services/OrderPackage.js";
import authJWT from "../middleware/authJWT.js";

/**
 * ORDER PACKAGE CONTROLLER
 * ==============================
 * Created by Harival Tivani
 * Created at Januari 01, 2021
 * ==============================
 */


export const create_order = async (req, res) => {
    const user_id = await authJWT.authData(req, res);
    return res.json(await create_order_paket_service(req.body, user_id));
}

export const pembayaran = async (req, res) => {
    const user_id = await authJWT.authData(req, res);
    return res.json(await pembayaran_service(req.params.id, user_id));
}

export const upload_bukti_pembayaran = async (req, res) => {
    const user_id = await authJWT.authData(req, res);
    return res.json(await upload_bukti_pembayaran_service(req, req.params.id, user_id));
}


/**
 * ORDER PACKAGE CONTROLLER SUPER ADMIN SIDE
 * =========================================
 * Created by Harival Tivani
 * Created at Januari 04, 2022
 * =========================================
 */

export const list_pembayaran = async (req, res) => {
    return res.json(await list_pembayaran_service());
}

export const detail_list_pembayaran = async (req, res) => {
    return res.json(await detail_list_pembayaran_service(req.params.id));
}

export const accept_pembayaran = async (req, res) => {
    const user_id = await authJWT.authData(req, res);
    return res.json(await accept_pembayaran_service(req.body, user_id));
}