import { 
    package_list_service,
    package_create_service,
    package_update_service,
    package_byid_service,
    package_delete_service,
    package_list_user_service
} from "../services/Package.js";
import authJWT from "../middleware/authJWT.js";
import { validationResult } from "express-validator";

/**
 * PACKAGE CONTROLLER
 * ==============================
 * Created by Harival Tivani
 * Created at December 27, 2021
 * ==============================
 */


export const package_list = async (req, res) => {
    let BASE_URL = req.protocol+"://"+req.headers.host+'/';
    return res.json(await package_list_service(BASE_URL));
}

export const package_create = async (req, res) => {
    const user_id = await authJWT.authData(req, res);
    const errors = validationResult(req).errors;
    if (errors.length > 0) {
        return res.status(400).json({error: errors})
    }
    else{
        return res.json(await package_create_service(req.body, user_id));
    }
}

export const package_update = async (req, res) => {
    const user_id = await authJWT.authData(req, res);
    const errors = validationResult(req).errors;
    if (errors.length > 0) {
        return res.status(400).json({error: errors})
    }
    else{
        return res.json(await package_update_service(req.body, req.params.id, user_id));
    }
}

export const package_byid = async (req, res) => {
    return res.json(await package_byid_service(req.params.id));
}

export const package_delete = async (req, res) => {
    const user_id = await authJWT.authData(req, res);
    return res.json(await package_delete_service(req.body.package_id, req.body.status, user_id));
}

/**
 * Route Accessing by user
 * ==============================
 * Created by Harival Tivani
 * Created at December 30, 2021
 * ==============================
 */
 export const package_list_user = async (req, res) => {
    return res.json(await package_list_user_service());
}
