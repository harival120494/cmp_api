import { 
    periode_list_service,
    periode_create_service,
    periode_update_service,
    periode_byid_service,
    periode_delete_service,
    periode_cs_service
} from "../services/Periode.js";
import authJWT from "../middleware/authJWT.js";
import { validationResult } from "express-validator";

/**
 * PERIODE CONTROLLER
 * ==============================
 * Created by Harival Tivani
 * Created at December 27, 2021
 * ==============================
 */


export const periode_list = async (req, res) => {
    return res.json(await periode_list_service());
}

export const periode_create = async (req, res) => {
    const user_id = await authJWT.authData(req, res);
    const errors = validationResult(req).errors;
    if (errors.length > 0) {
        return res.status(400).json({error: errors})
    }
    else{
        return res.json(await periode_create_service(req.body, user_id));
    }
}

export const periode_update = async (req, res) => {
    const user_id = await authJWT.authData(req, res);
    const errors = validationResult(req).errors;
    if (errors.length > 0) {
        return res.status(400).json({error: errors})
    }
    else{
        return res.json(await periode_update_service(req.body, req.params.id, user_id));
    }
}

export const periode_byid = async (req, res) => {
    return res.json(await periode_byid_service(req.params.id));
}

export const periode_delete = async (req, res) => {
    const user_id = await authJWT.authData(req, res);
    return res.json(await periode_delete_service(req.params.id, user_id));
}

export const periode_cs = async (req, res) => {
    const user_id = await authJWT.authData(req, res);
    return res.json(await periode_cs_service(req.body.periode_id, req.body.status, user_id));
}