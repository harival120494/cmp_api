import { 
    user_role_service,
    user_role_byid_service,
    create_user_role_service,
    update_user_role_service,
    role_cs_service
} from "../services/role.js";
import authJWT from "../middleware/authJWT.js";

/**
 * ROLE CONTROLLER
 * ==============================
 * Created by Harival Tivani
 * Created at December 29, 2021
 * ==============================
 */

export const user_role = async (req, res) => {
    return res.status(200).json(await user_role_service(req.body));
}

export const user_role_byid = async (req, res) => {
    return res.status(200).json(await user_role_byid_service(req.params.id));
}

export const role_create = async (req, res) => {
    const user_id = await authJWT.authData(req, res);
    return res.json(await create_user_role_service(req.body, user_id));
}

export const role_update = async (req, res) => {
    const user_id = await authJWT.authData(req, res);
    return res.json(await update_user_role_service(req.body, req.params.id, user_id));
}

// export const role_delete = async (req, res) => {
//     return res.json(await delete_user_role_service(req.params.id));
// }
export const role_delete = async (req, res) => {
    return res.json(await role_cs_service(req.body.id_role, req.body.status));
}