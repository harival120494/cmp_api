import { 
    get_authorization_shopee,
    store_authorization_shopee,
    store_token_shopee,
    get_shop_profile_service,
    get_category_list_service,
    get_product_list_service,
    delete_product_item_service,
    add_product_item_service,
    get_attributes_list_service,
    get_item_limit_service,
    get_item_base_info_service,
    get_item_extra_info_service,
    get_tier_variation_service,
    update_tier_variation_service,
    get_model_list_service,
    add_model_list_service,
    update_model_list_service,
    delete_model_list_service,
    update_price_service,
    update_stock_service,
    get_item_promotion_service,
    get_sip_item_price_service,
    search_item_service,
    category_recommend_service,
    get_order_list_service,
    get_shipment_list_service,
    get_order_detail_service,
    split_order_service,
    unsplit_order_service,
    cancel_order_service,
    handler_buyer_cancelation_service,
    add_invoice_data_service,
    get_return_list_service,
    get_return_detail_service,
    get_shipping_parameter_service,
    get_tracking_number_service,
    get_ship_order_service,
    update_shipping_order_service,
    get_shipping_document_parameter_service,
    create_shipping_document_service,
    get_shipping_document_result_service,
    get_tracking_info_service,
    get_address_list_service,
    get_channel_list_service,
    update_channel_service,
    batch_ship_order_service,
    get_unbind_order_list_service,
    get_detail_delivery_service,
    generate_first_mile_tracking_number_service,
    bind_first_mile_tracking_number_service,
    unbind_first_mile_tracking_number_service,
    get_tracking_number_list_service,
    get_waybill_service,
    get_channel_list_order_service
    
} from "../services/Shopee.js";
import authJWT from "../middleware/authJWT.js";
/**
 * SHOPEE CONTROLLER
 * ==============================
 * Created by Harival Tivani
 * Created at January 21, 2022
 * ==============================
 */


export const get_authorization_controller = async (req, res) => {
    
    return res.json(await get_authorization_shopee());
}
 export const save_authorization_controller = async (req, res) => {
    const user_id = await authJWT.authData(req, res);
    return res.json(await store_authorization_shopee(req.body, user_id));
}

export const save_token_controller = async (req, res) => {
    const user_id = await authJWT.authData(req, res);
    return res.json(await store_token_shopee(req.body, user_id));
}

export const get_shop_profile = async (req, res) => {
    const user_id = await authJWT.authData(req, res);
    return res.json(await get_shop_profile_service(user_id, req.params.shop_id));
}

export const get_product_list = async(req, res) => {
    const user_id = await authJWT.authData(req, res);
    const data = req.body
    const time_from = Number(Math.floor(new Date(data.update_time_from).valueOf() / 1000))
    const time_to = Number(Math.floor(new Date().valueOf(data.update_time_to) / 1000))
    return res.json(await get_product_list_service(req.params.shop_id, data, time_from, time_to, user_id))
}

export const add_product_item = async(req, res) => {
    const user_id = await authJWT.authData(req, res);
    return res.json(await add_product_item_service(req.params.shop_id, req.body,  user_id))
}

export const delete_product_item = async(req, res) => {
    const user_id = await authJWT.authData(req, res);
    return res.json(await delete_product_item_service(req.params.shop_id, req.params.item_id, user_id))
}

export const get_category_list = async(req, res) => {
    const user_id = await authJWT.authData(req, res);
    return res.json(await get_category_list_service(req.params.shop_id, user_id))
}

export const get_attributes_list = async(req, res) => {
    const user_id = await authJWT.authData(req, res);
    return res.json(await get_attributes_list_service(req.params.shop_id, req.params.category_id, user_id))
}

export const get_item_limit_list = async(req, res) => {
    const user_id = await authJWT.authData(req, res);
    return res.json(await get_item_limit_service(req.params.shop_id, user_id))
}

export const get_item_base_info_list = async(req, res) => {
    const user_id = await authJWT.authData(req, res);
    return res.json(await get_item_base_info_service(req.params.shop_id, req.params.item_id, user_id))
}

export const get_item_extra_info_list = async(req, res) => {
    const user_id = await authJWT.authData(req, res);
    return res.json(await get_item_extra_info_service(req.params.shop_id, req.params.item_id, user_id))
}

export const get_tier_variation_list = async(req, res) => {
    const user_id = await authJWT.authData(req, res);
    return res.json(await get_tier_variation_service(req.params.shop_id, req.body, user_id))
}

export const update_tier_variation_list = async(req, res) => {
    const user_id = await authJWT.authData(req, res);
    return res.json(await update_tier_variation_service(req.params.shop_id, req.body, user_id))
}

export const get_model_list = async(req, res) => {
    const user_id = await authJWT.authData(req, res);
    return res.json(await get_model_list_service(req.params.shop_id, req.params.item_id, user_id))
}

export const add_model_list = async(req, res) => {
    const user_id = await authJWT.authData(req, res);
    return res.json(await add_model_list_service(req.params.shop_id, req.body, user_id))
}

export const update_model_list = async(req, res) => {
    const user_id = await authJWT.authData(req, res);
    return res.json(await update_model_list_service(req.params.shop_id, req.body, user_id))
}

export const delete_model_list = async(req, res) => {
    const user_id = await authJWT.authData(req, res);
    return res.json(await delete_model_list_service(req.params.shop_id, req.body, user_id))
}

export const update_price = async(req, res) => {
    const user_id = await authJWT.authData(req, res);
    return res.json(await update_price_service(req.params.shop_id, req.body, user_id))
}

export const update_stock = async(req, res) => {
    const user_id = await authJWT.authData(req, res);
    return res.json(await update_stock_service(req.params.shop_id, req.body, user_id))
}

export const get_item_promotion = async(req, res) => {
    const user_id = await authJWT.authData(req, res);
    return res.json(await get_item_promotion_service(req.params.shop_id, req.params.item_id_list, user_id))
}

export const get_sip_item_price = async(req, res) => {
    const user_id = await authJWT.authData(req, res);
    return res.json(await get_sip_item_price_service(req.params.shop_id, req.body, user_id))
}

export const search_item = async(req, res) => {
    const user_id = await authJWT.authData(req, res);
    return res.json(await search_item_service(req.params.shop_id, req.params.page_size, req.params.item_name, user_id))
}

export const category_recommend = async(req, res) => {
    const user_id = await authJWT.authData(req, res);
    return res.json(await category_recommend_service(req.params.shop_id, req.params.item_name, user_id))
}

export const get_order_list = async(req, res) => {
    const user_id = await authJWT.authData(req, res);
    const data = req.body
    const time_from = Math.floor(new Date(data.time_from).valueOf() / 1000)
    const time_to = Math.floor(new Date().valueOf(data.time_to) / 1000)
    return res.json(await get_order_list_service(req.params.shop_id, data.time_range_field, time_from, time_to, data.page_size, user_id))
}

export const get_shipment_list = async(req, res) => {
    const user_id = await authJWT.authData(req, res);
    return res.json(await get_shipment_list_service(req.params.shop_id, req.params.page_size, user_id))
}

export const get_order_detail = async(req, res) => {
    const user_id = await authJWT.authData(req, res);
    return res.json(await get_order_detail_service(req.params.shop_id, req.params.order_sn_list, user_id))
}

export const split_order = async(req, res) => {
    const user_id = await authJWT.authData(req, res);
    return res.json(await split_order_service(req.params.shop_id, req.body, user_id))
}

export const unsplit_order = async(req, res) => {
    const user_id = await authJWT.authData(req, res);
    return res.json(await unsplit_order_service(req.params.shop_id, req.body, user_id))
}

export const cancel_order = async(req, res) => {
    const user_id = await authJWT.authData(req, res);
    return res.json(await cancel_order_service(req.params.shop_id, req.body, user_id))
}

export const handler_buyer_cancelation = async(req, res) => {
    const user_id = await authJWT.authData(req, res);
    return res.json(await handler_buyer_cancelation_service(req.params.shop_id, req.body, user_id))
}

export const add_invoice_data = async(req, res) => {
    const user_id = await authJWT.authData(req, res);
    return res.json(await add_invoice_data_service(req.params.shop_id, req.body, user_id))
}

export const get_return_list = async(req, res) => {
    const user_id = await authJWT.authData(req, res);
    const data = req.body
    const time_from = Math.floor(new Date(data.create_time_from).valueOf() / 1000)
    const time_to = Math.floor(new Date().valueOf(data.create_time_to) / 1000)
    return res.json(await get_return_list_service(req.params.shop_id, data.page_no, data.page_size, time_from, time_to, user_id))
}

export const get_return_detail = async(req, res) => {
    const user_id = await authJWT.authData(req, res);
    return res.json(await get_return_detail_service(req.params.shop_id, req.params.return_sn, user_id))
}

export const get_shipping_parameter = async(req, res) => {
    const user_id = await authJWT.authData(req, res);
    return res.json(await get_shipping_parameter_service(req.params.shop_id, req.params.order_sn, user_id))
}

export const get_tracking_number = async(req, res) => {
    const user_id = await authJWT.authData(req, res);
    return res.json(await get_tracking_number_service(req.params.shop_id, req.body, user_id))
}

export const get_ship_order = async(req, res) => {
    const user_id = await authJWT.authData(req, res);
    return res.json(await get_ship_order_service(req.params.shop_id, req.body, user_id))
}

export const update_shipping_order = async(req, res) => {
    const user_id = await authJWT.authData(req, res);
    return res.json(await update_shipping_order_service(req.params.shop_id, req.body, user_id))
}

export const get_shipping_document_parameter = async(req, res) => {
    const user_id = await authJWT.authData(req, res);
    return res.json(await get_shipping_document_parameter_service(req.params.shop_id, req.body, user_id))
}

export const create_shipping_document = async(req, res) => {
    const user_id = await authJWT.authData(req, res);
    return res.json(await create_shipping_document_service(req.params.shop_id, req.body, user_id))
}

export const get_shipping_document_result = async(req, res) => {
    const user_id = await authJWT.authData(req, res);
    return res.json(await get_shipping_document_result_service(req.params.shop_id, req.body, user_id))
}

export const get_tracking_info = async(req, res) => {
    const user_id = await authJWT.authData(req, res);
    return res.json(await get_tracking_info_service(req.params.shop_id, req.params.order_sn, req.params.package_number, user_id))
}

export const get_address_list = async(req, res) => {
    const user_id = await authJWT.authData(req, res);
    return res.json(await get_address_list_service(req.params.shop_id, user_id))
}

export const get_channel_list = async(req, res) => {
    const user_id = await authJWT.authData(req, res);
    return res.json(await get_channel_list_service(req.params.shop_id, user_id))
}

export const update_channel = async(req, res) => {
    const user_id = await authJWT.authData(req, res);
    return res.json(await update_channel_service(req.params.shop_id, req.body, user_id))
}

export const batch_ship_order = async(req, res) => {
    const user_id = await authJWT.authData(req, res);
    return res.json(await batch_ship_order_service(req.params.shop_id, req.body, user_id))
}

export const get_unbind_order_list = async(req, res) => {
    const user_id = await authJWT.authData(req, res);
    return res.json(await get_unbind_order_list_service(req.params.shop_id, req.body, user_id))
}

export const get_detail_delivery = async(req, res) => {
    const user_id = await authJWT.authData(req, res);
    return res.json(await get_detail_delivery_service(req.params.shop_id, req.body, user_id))
}

export const generate_first_mile_tracking_number = async(req, res) => {
    const user_id = await authJWT.authData(req, res);
    return res.json(await generate_first_mile_tracking_number_service(req.params.shop_id, req.body, user_id))
}

export const bind_first_mile_tracking_number = async(req, res) => {
    const user_id = await authJWT.authData(req, res);
    return res.json(await bind_first_mile_tracking_number_service(req.params.shop_id, req.body, user_id))
}

export const unbind_first_mile_tracking_number = async(req, res) => {
    const user_id = await authJWT.authData(req, res);
    return res.json(await unbind_first_mile_tracking_number_service(req.params.shop_id, req.body, user_id))
}

export const get_tracking_number_list = async(req, res) => {
    const user_id = await authJWT.authData(req, res);
    return res.json(await get_tracking_number_list_service(req.params.shop_id, req.body, user_id))
}

export const get_waybill = async(req, res) => {
    const user_id = await authJWT.authData(req, res);
    return res.json(await get_waybill_service(req.params.shop_id, req.body, user_id))
}

export const get_channel_list_order = async(req, res) => {
    const user_id = await authJWT.authData(req, res);
    return res.json(await get_channel_list_order_service(req.params.shop_id, req.params.region, user_id))
}
