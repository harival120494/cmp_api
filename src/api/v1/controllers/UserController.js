import {
    login as login_service,
    all_user as all_user_service,
    register_service,
    confirm_otp_service,
    reset_otp_service,
    user_role_service,
    register_byadmin_service,
    user_byid_service,
    update_user_service
} from '../services/User.js';
import { validationResult } from "express-validator";

/**
 * USER CONTROLLER
 * ==============================
 * Created by Harival Tivani
 * Created at December 24, 2021
 * ==============================
 */


export const login = async (req, res) => {
    const results = await login_service(req.body);
    return res.json(results);
}


export const register = async (req, res) => {
    const errors = validationResult(req).errors;
    if (errors.length > 0) {
        return res.status(400).json({error: errors})
    }
    else{
        return res.status(201).json(await register_service(req.body));
    }
}

export const confirm_otp = async (req, res) => {
    return res.status(200).json(await confirm_otp_service(req.params.id));
}

export const reset_otp = async (req, res) => {
    return res.status(200).json(await reset_otp_service(req.params.id));
}

export const all_user = async (req, res) => {
    return res.status(200).json(await all_user_service(req.body));
}

export const user_role = async (req, res) => {
    return res.status(200).json(await user_role_service(req.body));
}

export const register_byadmin = async (req, res) => {
    const errors = validationResult(req).errors;
    if (errors.length > 0) {
        return res.status(400).json({error: errors})
    }
    else{
        return res.status(201).json(await register_byadmin_service(req.body));
    }
}

export const user_byid = async (req, res) => {
    return res.status(200).json(await user_byid_service(req.params.id));
}

export const update_user = async (req, res) => {
    return res.status(200).json(await update_user_service(req.body, req.params.id));
}