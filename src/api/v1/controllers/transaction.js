import authJWT from "../middleware/authJWT.js";
import { 
    list_toko_service,
    list_paket_user_service,
    detail_package_order_service,
    data_invoice_service,
    update_profile_toko_service
} from "../services/transaction.js";

/**
 * TRANSACTION CONTROLLER
 * ==============================
 * Created by Harival Tivani
 * Created at January 25, 2022
 * ==============================
 */


export const get_list_toko = async (req, res) => {
    const user_id = await authJWT.authData(req, res);
    return res.json(await list_toko_service(user_id));
}

export const get_list_paket = async (req, res) => {
    const user_id = await authJWT.authData(req, res);
    return res.json(await list_paket_user_service(user_id));
}

export const detail_package_order = async (req, res) => {
    const user_id = await authJWT.authData(req, res);
    let BASE_URL = req.protocol+"://"+req.headers.host+'/';
    return res.json(await detail_package_order_service(req.params.package_order_id, user_id, BASE_URL));
}

export const detail_invoice = async (req, res) => {
    const user_id = await authJWT.authData(req, res);
    return res.json(await data_invoice_service(req.params.package_order_id, user_id));
}

export const update_profile_toko = async (data, res, type_file) => {
    const user_id = await authJWT.authData(data);
    let shop_logo = data.protocol+"://"+data.headers.host+'/files/uploads/shop_logo/'+user_id+'.'+type_file;
    let shop_id = data.params.shop_id;
    // return res.json(await update_profile_toko_service(data.body, shop_logo, shop_id, user_id));
}

