import DShop from "../models/DShopModel.js";
import moment from 'moment';
import CryptoJS from "crypto-js";
import ShopeeAPI from "../../../../const/Shopee.js";
import axios from "axios";

export const AccessToken = async (user_id, shop_id) => {
    const data = await DShop.findOne(
                { 
                    where : {
                        user_id : user_id, 
                        shop_id_marketplace : shop_id
                    }
                },
                {
                    type : DShop.sequelize.QueryTypes.SELECT
                }
                ).then((result) => {
                    return result
                })
    const last_update   = String(moment().valueOf(data.updated_date)).substring(0,10)
    const expire_in     = data.expire_in
    const will_expire   = parseInt(last_update) + parseInt(expire_in);
    const now           = String(moment().valueOf()).substring(0,10)

    if(now > will_expire){
        const refresh_token = data.refresh_token
        const partner_id    = Number(ShopeeAPI.SHOPEE_PARTNER_ID)
        const partner_key   = ShopeeAPI.SHOPEE_PARTNER_KEY
        const shop_id       = data.shop_id_marketplace
        const host          = ShopeeAPI.SHOPEE_AUTH_HOST
        const path          = ShopeeAPI.SHOPEE_REFRESH_TOKEN
        const baseString    = `${partner_id}${path}${now}`
        const sign          = CryptoJS.HmacSHA256(baseString, partner_key).toString()

        const header = {headers : {"Content-Type": "application/json"}}
        const body = {
            shop_id         : shop_id,
            refresh_token   : refresh_token,
            partner_id      : partner_id
        }
        
        const api = `${host}${path}?partner_id=${partner_id}&timestamp=${now}&sign=${sign}`;
        return await axios.post(api, body, header).then( async(result) => {
            if(result){
                const token =  result.data.access_token
                return await DShop.update(
                    { 
                        request_id      : result.data.request_id,
                        access_token    : result.data.access_token, 
                        refresh_token   : result.data.refresh_token, 
                        expire_in       : result.data.expire_in,
                        updated_date    : moment().format('YYYY-MM-DD HH:mm:ss')
                    }, 
                    {
                        where: { shop_id_marketplace : shop_id },
                        type : DShop.sequelize.QueryTypes.update
                    })
                .then(results => {
                    if(results){return ({status : 200, token : result.data.access_token })}
                    else{return ({status : 400, data : 'Data update failed'})}
                })
            }
        })
    }
    else{
        return ({status : 200, token : data.access_token })
    }
    
    
}