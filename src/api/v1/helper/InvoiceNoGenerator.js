import OrderPackage from "../models/OrderPaket.js";
import { Sequelize } from 'sequelize';

export const InvoiceGenerator = async (user_id) => {
    try {
        return OrderPackage.sequelize.query(`SELECT 
                    top.no_invoice,
                    DATE_FORMAT(NOW(), '%m') as bulan,
                    DATE_FORMAT(NOW(), '%y') as tahun,
                    COALESCE(MAX(RIGHT(top.no_invoice, 5)), 0, MAX(RIGHT(top.no_invoice, 5)))  as last_seq
                FROM tr_order_package as top
                WHERE top.user_id = $1
                AND substr(top.no_invoice, -8, 2) = DATE_FORMAT(NOW(), '%m')
                AND substr(top.no_invoice, -11, 2) = DATE_FORMAT(NOW(), '%y') `,
            { bind: [user_id],type: Sequelize.QueryTypes.SELECT }
        ).then(function(result) {
            const last = ''+(parseInt(result[0].last_seq) + 1);
            const no_invoice = `INV/${user_id}/${result[0].tahun}/${result[0].bulan}/${last.padStart(5,0)}`;
            return no_invoice.padStart(5,0);
        });
    } catch (error) {
        return error;
    }
}