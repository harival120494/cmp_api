import axios from 'axios'
import dotenv from'dotenv';
dotenv.config()

const APP_MODE = process.env.APP_MODE;
export const ShopeeAuth = (data) => {
    let host = '';
    let time = Math.floor(Date.now() / 1000);

    switch (APP_MODE) {
        case 'DEV':
            host = process.env.SHOPEE_AUTH_URL_DEV;
            break;
    
        default:
            host = process.env.SHOPEE_AUTH_URL_PROD;
            break;
    }    
}