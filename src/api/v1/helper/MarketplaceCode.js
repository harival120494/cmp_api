import Marketplace from "../models/MarketplaceModel.js";
import { Sequelize } from 'sequelize';

export const MarketplaceCodeGenerator = async () => {
    try {
        const query = {
            attributes: [
              [Sequelize.literal(`COALESCE(MAX(RIGHT(marketplace_code, 3)), 0)`), 'last_seq'],
            ],
            raw: true,
           type :  Marketplace.sequelize.QueryTypes.SELECT
        };
        return Marketplace.findOne(query).then(result => {
            const last = ''+(parseInt(result.last_seq) + 1);
            return last.padStart(3,0);
        });
    } catch (error) {
        return error;
    }
}