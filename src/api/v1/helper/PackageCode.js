import Package from "../models/PackageModel.js";
import { Sequelize } from 'sequelize';

export const PackageCodeGenerator = async () => {
    try {
        const query = {
            attributes: [
              [Sequelize.literal(`COALESCE(MAX(RIGHT(package_code, 3)),0)`), 'last_seq'],
            ],
            raw: true,
           type :  Package.sequelize.QueryTypes.SELECT
        };
        return Package.findOne(query).then(result => {
            const last = ''+(parseInt(result.last_seq) + 1);
            return last.padStart(3,0);
        });
    } catch (error) {
        return error;
    }
}