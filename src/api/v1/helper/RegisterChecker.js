import User from "../models/UserModel.js";

export const RegisterChecker = async (param, value) => {
    try {
        if(param == 'email'){
            const results = await User.findOne({where : {email : value}})
            return results;
        }
        else if(param == 'nohp'){
            const results = await User.findOne({where : {nohp : value}})
            return results;
        }
    } catch (error) {
        return error;
    }
}