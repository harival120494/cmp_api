import bcrypt from "bcrypt";
import dotenv from "dotenv";
import moment from 'moment';
import DShop from "../models/DShopModel.js";
import { Sequelize } from 'sequelize';
import ShopeeAPI from "../../../../const/Shopee.js";
import axios from 'axios';
dotenv.config();


export const ShopeeGetShopProfile = async (data) => {
    
    const header    = {headers : {"Content-Type": "application/json"}}
    const api       = `${ShopeeAPI.SHOPEE_AUTH_HOST}${ShopeeAPI.SHOPEE_GET_SHOP_PROFILE}?shop_id=${Number(data.shop_id)}&partner_id=${Number(data.partner_id)}&access_token=${data.access_token}&sign=${data.sign}&timestamp=${data.timestamp}`
    return await axios.get(api, header).then(async (result) => {
        return await DShop.update(
            { 
                shop_logo : result.data.response.shop_logo,
                shop_name : result.data.response.shop_name
            }, 
            {
                where: { shop_id_marketplace : data.shop_id },
                type : DShop.sequelize.QueryTypes.update
            })
        .then(result1 => {
            if(result1){return ({status : 200, data : 'Synchronisasi data toko berhasil'})}
            else{return ({status : 400, data : 'Data update failed'})}            
        })
    })
}