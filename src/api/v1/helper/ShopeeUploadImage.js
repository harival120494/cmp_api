import dotenv from "dotenv";
import ShopeeAPI from "../../../../const/Shopee.js";
import axios from 'axios';
import { AccessToken } from "./AccessToken.js";
import CryptoJS from "crypto-js";
import FormData from 'form-data'
dotenv.config();

/**
 * SHOPEE UPLOAD IMAGE HELPER
 * ==============================
 * Created by Harival Tivani
 * Created at January 25, 2022
 * ==============================
 */

const timestamp = Math.floor(new Date().valueOf() / 1000)
const partner_id = Number(ShopeeAPI.SHOPEE_PARTNER_ID)
const partner_key = ShopeeAPI.SHOPEE_PARTNER_KEY
const host = ShopeeAPI.SHOPEE_AUTH_HOST

export const ShopeeUploadImage = async (file, user_id, shop_id) => {
    
    const access_token = await AccessToken(user_id, shop_id)
    let base_string = `${partner_id}${ShopeeAPI.UPLOAD_IMAGE}${Number(timestamp)}`;
    let sign        = CryptoJS.HmacSHA256(base_string, partner_key).toString()
    const header    = { headers: { 'Content-Type': 'multipart/form-data'} }
    // const header = {headers : file.getHeaders()}
    const api       = `${host}${ShopeeAPI.UPLOAD_IMAGE}?partner_id=${partner_id}&sign=${sign}&timestamp=${timestamp}`
    console.log(file)
    // return await axios.post(api, file, header).then(async (result) => {
    //     return result;
    // })
}