import multer from 'multer';

// menentukan lokasi pengunggahan
export  const uploadPembayaran = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, '../../public/uploads/');
    },
    filename: (req, file, cb) => {
        cb(null, Date.now()+'-'+file.originalname);
    }
});