import jwt from "jsonwebtoken";
import dotenv from "dotenv";
dotenv.config();

const GenerateToken = (data) => {
    const jwt_token = new Promise((resolve, reject) => {
        jwt.sign(
            {
                user_id : data.user_id,
                email : data.email,
                password : data.password,
                role_id : data.role_id
            },
            process.env.JWT_SECRET, 
            {
                expiresIn: '1d'
            },
            (err, token) => {
                if (err) reject(err)
                
                resolve (
                    {
                        status : 200,
                        pic_name : data.pic_name,
                        user_id : data.user_id,
                        email : data.email,
                        role_id : data.role_id,
                        token : token
                    }
                )
            }
        );
    });
    return jwt_token;
}

export default GenerateToken;