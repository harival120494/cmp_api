import { Sequelize } from 'sequelize';
import conn from '../../../../config/db.js';

/**
 * BANK MODEL
 * ==============================
 * Created by Harival Tivani
 * Created at December 30, 2021
 * ==============================
 */

const sequelize = new Sequelize('sqlite::memory:');
const { DataTypes } = Sequelize;

const Bank = conn.define('c_bank', {
    bank_id : {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER,
    },
    bank_code : {
        type : DataTypes.STRING,
        allowNull : true
    },
    bank_name : {
        type : DataTypes.STRING,
        allowNull : true
    },
    bank_account_number : {
        type : DataTypes.STRING,
        allowNull : true
    },
    bank_account_name : {
        type : DataTypes.STRING,
        allowNull : true
    },
    status : {
        type : DataTypes.INTEGER,
        allowNull : true
    },   
    created_date : {
        type : DataTypes.DATE,
        allowNull : true
    },
    updated_date : {
        type : DataTypes.DATE,
        allowNull : true
    },
    created_by : {
        type : DataTypes.INTEGER,
        allowNull : true
    },
    updated_by : {
        type : DataTypes.INTEGER,
        allowNull : true
    },
}, {
    freezeTableName : true,
    timestamps: false
});

Bank.removeAttribute('id');
export default Bank;