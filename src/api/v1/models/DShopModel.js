import { Sequelize } from 'sequelize';
import conn from '../../../../config/db.js';

/**
 * BANK MODEL
 * ==============================
 * Created by Harival Tivani
 * Created at January 21, 2022
 * ==============================
 */

const sequelize = new Sequelize('sqlite::memory:');
const { DataTypes } = Sequelize;

const DShop = conn.define('d_shop', {
    shop_id : {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER,
    },
    user_id : {
        type : DataTypes.INTEGER,
        allowNull : false
    },
    shop_name : {
        type : DataTypes.STRING,
        allowNull : true
    },
    shop_logo : {
        type : DataTypes.STRING,
        allowNull : true
    },
    description : {
        type : DataTypes.STRING,
        allowNull : true
    },
    email : {
        type : DataTypes.STRING,
        allowNull : true
    },
    password : {
        type : DataTypes.STRING,
        allowNull : true
    },
    marketplace_id : {
        type : DataTypes.INTEGER,
        allowNull : true
    },   
    status : {
        type : DataTypes.INTEGER,
        allowNull : true
    },
    shop_id_marketplace : {
        type : DataTypes.INTEGER,
        allowNull : true
    },
    code_marketplace : {
        type : DataTypes.STRING,
        allowNull : true
    },
    main_account_id : {
        type : DataTypes.STRING,
        allowNull : true
    },
    request_id : {
        type : DataTypes.STRING,
        allowNull : true
    },
    refresh_token : {
        type : DataTypes.STRING,
        allowNull : true
    },
    access_token : {
        type : DataTypes.STRING,
        allowNull : true
    },
    expire_in : {
        type : DataTypes.STRING,
        allowNull : true
    },
    created_date : {
        type : DataTypes.DATE,
        allowNull : true
    },
    updated_date : {
        type : DataTypes.DATE,
        allowNull : true
    },
    create_user : {
        type : DataTypes.INTEGER,
        allowNull : true
    },
    update_user : {
        type : DataTypes.INTEGER,
        allowNull : true
    }
}, {
    freezeTableName : true,
    timestamps: false
});

DShop.removeAttribute('id');
export default DShop;