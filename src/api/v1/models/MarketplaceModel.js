import { Sequelize } from 'sequelize';
import conn from '../../../../config/db.js';

/**
 * MARKETPLACE MODEL
 * ==============================
 * Created by Harival Tivani
 * Created at December 27, 2021
 * ==============================
 */

const sequelize = new Sequelize('sqlite::memory:');
const { DataTypes } = Sequelize;

const Marketplace = conn.define('c_marketplace', {
    marketplace_id : {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER,
    },
    marketplace_code : {
        type : DataTypes.STRING,
        allowNull : true
    },
    marketplace_name : {
        type : DataTypes.STRING,
        allowNull : true
    },    
    desc : {
        type : DataTypes.STRING,
        allowNull : true
    },  
    status : {
        type : DataTypes.INTEGER,
        allowNull : true
    },   
    created_date : {
        type : DataTypes.DATE,
        allowNull : true
    },
    updated_date : {
        type : DataTypes.DATE,
        allowNull : true
    },
    created_user : {
        type : DataTypes.INTEGER,
        allowNull : true
    },
    updated_user : {
        type : DataTypes.INTEGER,
        allowNull : true
    },
    logo_url : {
        type : DataTypes.STRING,
        allowNull : true
    },
    url_login : {
        type : DataTypes.STRING,
        allowNull : true
    }
}, {
    freezeTableName : true,
    timestamps: false
});

Marketplace.removeAttribute('id');
export default Marketplace;