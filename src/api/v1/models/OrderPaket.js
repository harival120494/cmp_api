import { Sequelize } from 'sequelize';
import conn from '../../../../config/db.js';

/**
 * ORDER PACKAGE MODEL
 * ==============================
 * Created by Harival Tivani
 * Created at Januari 01, 2022
 * ==============================
 */

const sequelize = new Sequelize('sqlite::memory:');
const { DataTypes } = Sequelize;

const OrderPackage = conn.define('tr_order_package', {
    order_package_id : {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER,
    },
    user_id : {
        type : DataTypes.INTEGER,
        allowNull : true
    },
    bank_id : {
        type : DataTypes.INTEGER,
        allowNull : true
    },    
    status_id : {
        type : DataTypes.INTEGER,
        allowNull : true
    },   
    no_invoice : {
        type : DataTypes.STRING,
        allowNull : true
    },  
    jumlah_toko : {
        type : DataTypes.INTEGER,
        allowNull : true
    },   
    harga_pertoko : {
        type : DataTypes.INTEGER,
        allowNull : true
    },  
    approve_date : {
        type : DataTypes.DATE,
        allowNull : true
    }, 
    approve_by : {
        type : DataTypes.INTEGER,
        allowNull : true
    }, 
    image_url : {
        type : DataTypes.STRING,
        allowNull : true
    },
    tgl_pembayaran : {
        type : DataTypes.DATE,
        allowNull : true
    },
    tgl_upload_bukti : {
        type : DataTypes.DATE,
        allowNull : true
    },
    created_date : {
        type : DataTypes.DATE,
        allowNull : true
    },
    update_date : {
        type : DataTypes.DATE,
        allowNull : true
    },
    create_user : {
        type : DataTypes.INTEGER,
        allowNull : true
    },
    update_user : {
        type : DataTypes.INTEGER,
        allowNull : true
    }
}, {
    freezeTableName : true,
    timestamps: false
});

OrderPackage.removeAttribute('id');
export default OrderPackage;