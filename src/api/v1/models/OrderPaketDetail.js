import { Sequelize } from 'sequelize';
import conn from '../../../../config/db.js';

/**
 * ORDER PACKAGE MODEL
 * ==============================
 * Created by Harival Tivani
 * Created at Januari 01, 2022
 * ==============================
 */

const sequelize = new Sequelize('sqlite::memory:');
const { DataTypes } = Sequelize;

const OrderPackageDetail = conn.define('tr_detail_order_package', {
    detail_order_package_id : {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER,
    },
    package_id : {
        type : DataTypes.INTEGER,
        allowNull : true
    },
    order_package_id : {
        type : DataTypes.INTEGER,
        allowNull : true
    }, 
    exp_date : {
        type : DataTypes.DATE,
        allowNull : true
    }, 
    harga_satuan : {
        type : DataTypes.INTEGER,
        allowNull : true
    },   
    qty : {
        type : DataTypes.INTEGER,
        allowNull : true
    },  
    status : {
        type : DataTypes.INTEGER,
        allowNull : true
    },   
    created_date : {
        type : DataTypes.DATE,
        allowNull : true
    },
    updated_date : {
        type : DataTypes.DATE,
        allowNull : true
    },
    create_user : {
        type : DataTypes.INTEGER,
        allowNull : true
    },
    update_user : {
        type : DataTypes.INTEGER,
        allowNull : true
    }
}, {
    freezeTableName : true,
    timestamps: false
});

OrderPackageDetail.removeAttribute('id');
export default OrderPackageDetail;