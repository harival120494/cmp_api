import { Sequelize } from 'sequelize';
import conn from '../../../../config/db.js';

/**
 * PACKAGE MODEL
 * ==============================
 * Created by Harival Tivani
 * Created at December 27, 2021
 * ==============================
 */

const sequelize = new Sequelize('sqlite::memory:');
const { DataTypes } = Sequelize;

const Package = conn.define('d_package', {
    package_id : {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER,
    },
    periode_id : {
        type : DataTypes.INTEGER,
        allowNull : true
    },
    marketplace_id : {
        type : DataTypes.INTEGER,
        allowNull : true
    },    
    package_code : {
        type : DataTypes.STRING,
        allowNull : false
    },   
    package_name : {
        type : DataTypes.STRING,
        allowNull : false
    },
    price : {
        type : DataTypes.INTEGER,
        allowNull : false
    },
    start_date : {
        type : DataTypes.DATE,
        allowNull : false
    },
    end_date : {
        type : DataTypes.DATE,
        allowNull : false
    },
    desc : {
        type : DataTypes.STRING,
        allowNull : false
    },
    created_date : {
        type : DataTypes.DATE,
        allowNull : true
    },
    updated_date : {
        type : DataTypes.DATE,
        allowNull : true
    },
    status : {
        type : DataTypes.INTEGER,
        allowNull : false
    },
    create_user : {
        type : DataTypes.INTEGER,
        allowNull : true
    },
    update_user : {
        type : DataTypes.INTEGER,
        allowNull : true
    },
}, {
    freezeTableName : true,
    timestamps: false
});

Package.removeAttribute('id');
export default Package;