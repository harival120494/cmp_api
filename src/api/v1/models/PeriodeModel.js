import { Sequelize } from 'sequelize';
import conn from '../../../../config/db.js';

/**
 * PERIODE MODEL
 * ==============================
 * Created by Harival Tivani
 * Created at December 27, 2021
 * ==============================
 */

const sequelize = new Sequelize('sqlite::memory:');
const { DataTypes } = Sequelize;

const Periode = conn.define('c_periode', {
    periode_id : {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER,
    },
    periode_name : {
        type : DataTypes.STRING,
        allowNull : true
    },
    jumlah_hari : {
        type : DataTypes.INTEGER,
        allowNull : true
    },    
    status : {
        type : DataTypes.INTEGER,
        allowNull : true
    },   
    created_date : {
        type : DataTypes.DATE,
        allowNull : true
    },
    updated_date : {
        type : DataTypes.DATE,
        allowNull : true
    },
    created_user : {
        type : DataTypes.INTEGER,
        allowNull : true
    },
    updated_user : {
        type : DataTypes.INTEGER,
        allowNull : true
    },
}, {
    freezeTableName : true,
    timestamps: false
});

Periode.removeAttribute('id');
export default Periode;