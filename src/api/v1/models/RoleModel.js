import { Sequelize } from 'sequelize';
import conn from '../../../../config/db.js';

const sequelize = new Sequelize('sqlite::memory:');
const { DataTypes } = Sequelize;

const Role = conn.define('role', {
    id_role : {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER,
    },
    role_name : {
        type : DataTypes.STRING,
        allowNull : true
    },
    kode_role : {
        type : DataTypes.STRING,
        allowNull : true
    },
    desc : {
        type : DataTypes.STRING,
        allowNull : true
    },    
    status : {
        type : DataTypes.INTEGER,
        allowNull : true
    },
    created_at : {
        type : DataTypes.DATE,
        allowNull : true
    },
    updated_at : {
        type : DataTypes.DATE,
        allowNull : true
    },
    created_by : {
        type : DataTypes.INTEGER,
        allowNull : true
    },
    updated_by : {
        type : DataTypes.INTEGER,
        allowNull : true
    }
}, {
    freezeTableName : true,
    timestamps: false
});

Role.removeAttribute('id');
export default Role;