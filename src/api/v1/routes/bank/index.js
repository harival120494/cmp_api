import express from 'express';
import { packageValidator } from "../../middleware/validator.js";
import authJWT from "../../middleware/authJWT.js";
import { 
    list_bank,
    create_bank,
    update_bank,
    delete_bank,
    get_byid
 } from "../../controllers/BankController.js";

/**
 * BANK ROUTER
 * ==============================
 * Created by Harival Tivani
 * Created at December 30, 2021
 * ==============================
 */

const Bank = express.Router();

Bank.get('/',[authJWT.verifyToken],list_bank);
Bank.get('/:id',[authJWT.verifyToken],get_byid);
Bank.post('/',[authJWT.verifyToken],create_bank);
Bank.put('/:id',[authJWT.verifyToken],update_bank);
Bank.post('/change-status',[authJWT.verifyToken],delete_bank);


export default Bank;