import express from 'express';
import multer from "multer";
import { marketplaceValidator } from "../../middleware/validator.js";
import authJWT from "../../middleware/authJWT.js";
import { 
    marketplace_list,
    marketplace_create,
    marketplace_update,
    marketplace_byid,
    marketplace_delete,
    marketplace_list_byuser
} from "../../controllers/MarketPlaceController.js";


/**
 * PACKAGE ROUTER
 * ==============================
 * Created by Harival Tivani
 * Created at December 27, 2021
 * ==============================
 */

 const d = new Date();
 const storage = multer.diskStorage({
     destination: (req, file, cb) => {
         cb(null, 'public/uploads/marketplace_logo');
     },
     filename: (req, file, cb) => {
         cb(null,file.originalname);
     }
 });
 
const upload = multer({ storage });

const Marketplace = express.Router();

Marketplace.get('/',[authJWT.verifyToken], marketplace_list);
Marketplace.post('/',[authJWT.verifyToken, marketplaceValidator(), upload.single('logo')], marketplace_create);
Marketplace.put('/:id',[authJWT.verifyToken, marketplaceValidator(), upload.single('logo')], marketplace_update);
Marketplace.get('/:id',[authJWT.verifyToken], marketplace_byid);
Marketplace.post('/change-status',[authJWT.verifyToken], marketplace_delete);



export default Marketplace;