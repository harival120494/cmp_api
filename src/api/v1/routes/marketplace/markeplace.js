import express from 'express';
import authJWT from "../../middleware/authJWT.js";
import { 
    marketplace_list_byuser
} from "../../controllers/MarketPlaceController.js";


const MarketplaceUser = express.Router();


/** Route by user */
MarketplaceUser.get('/marketplace-list',[authJWT.verifyToken], marketplace_list_byuser);

export default MarketplaceUser;