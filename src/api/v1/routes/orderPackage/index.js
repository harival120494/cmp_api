import express from 'express';
import multer from "multer";
import authJWT from "../../middleware/authJWT.js";
import { 
    create_order,
    pembayaran,
    upload_bukti_pembayaran,
    list_pembayaran,
    detail_list_pembayaran,
    accept_pembayaran
} from "../../controllers/OrderPackageController.js";

/**
 * ORDER PACKAGE ROUTER
 * ==============================
 * Created by Harival Tivani
 * Created at Januari 27, 2021
 * ==============================
 */

const d = new Date();
const format = `${d.getYear()}${d.getMonth() + 1}${d.getDate()}${d.getHours()}${d.getMinutes()}`;
const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, 'public/uploads/');
    },
    filename: (req, file, cb) => {
        cb(null, format+''+file.originalname);
    }
});

const upload = multer({ storage });

const OrderPackage = express.Router();

OrderPackage.post('/',[authJWT.verifyToken], create_order);
OrderPackage.get('/pembayaran/:id',[authJWT.verifyToken], pembayaran);
OrderPackage.post('/upload_bukti_pembayaran/:id',[ authJWT.verifyToken, upload.single('gambar')], upload_bukti_pembayaran);

/**
 * Order Package Super Admin Side
 * =============================
 * Created by Harival Tivani
 * Created at Januari 04, 2022
 * =============================
 */

OrderPackage.get('/list-pembayaran/',[authJWT.verifyTokenAdmin], list_pembayaran);
OrderPackage.get('/detail-list-pembayaran/:id',[authJWT.verifyTokenAdmin], detail_list_pembayaran);
OrderPackage.post('/konfirmasi-pembayaran',[authJWT.verifyTokenAdmin], accept_pembayaran);


export default OrderPackage;