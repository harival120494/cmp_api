import express from 'express';
import { packageValidator } from "../../middleware/validator.js";
import authJWT from "../../middleware/authJWT.js";
import { 
    package_list,
    package_create,
    package_update,
    package_byid,
    package_delete,
    package_list_user
} from "../../controllers/PackageController.js";

/**
 * PACKAGE ROUTER
 * ==============================
 * Created by Harival Tivani
 * Created at December 27, 2021
 * ==============================
 */

const Package = express.Router();

Package.get('/',[authJWT.verifyToken], package_list);
Package.post('/',[authJWT.verifyToken, packageValidator()], package_create);
Package.put('/:id',[authJWT.verifyToken,  packageValidator()], package_update);
Package.get('/by-id/:id',[authJWT.verifyToken], package_byid);
Package.post('/change-status',[authJWT.verifyToken], package_delete);

/**
 * Route accessing by user
 * ==============================
 * Created by Harival Tivani
 * Created at December 30, 2021
 * ==============================
 */
 Package.get('/package-list/',[authJWT.verifyToken], package_list_user);

export default Package;