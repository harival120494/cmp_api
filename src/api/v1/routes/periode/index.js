import express from 'express';
import { periodeValidator } from "../../middleware/validator.js";
import authJWT from "../../middleware/authJWT.js";
import { 
    periode_list,
    periode_create,
    periode_update,
    periode_byid,
    periode_delete,
    periode_cs
} from "../../controllers/PeriodeController.js";

/**
 * PERIODE ROUTER
 * ==============================
 * Created by Harival Tivani
 * Created at December 27, 2021
 * ==============================
 */

const Periode = express.Router();

Periode.get('/',[authJWT.verifyToken], periode_list);
Periode.post('/',[authJWT.verifyToken, periodeValidator()], periode_create);
Periode.put('/:id',[authJWT.verifyToken, periodeValidator()], periode_update);
Periode.get('/:id',[authJWT.verifyToken], periode_byid);
Periode.delete('/:id',[authJWT.verifyToken], periode_delete);
Periode.post('/change-status',[authJWT.verifyToken], periode_cs);

export default Periode;