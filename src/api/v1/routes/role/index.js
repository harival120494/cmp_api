import express from 'express';
import {
    user_role,
    user_role_byid,
    role_create,
    role_update,
    role_delete
} from'../../controllers/RoleController.js'
import authJWT from "../../middleware/authJWT.js";

const Role = express.Router();

Role.get('/', [authJWT.verifyToken], user_role);
Role.get('/:id', [authJWT.verifyToken],  user_role_byid);
Role.post('/', [authJWT.verifyToken],  role_create);
Role.put('/:id', [authJWT.verifyToken],  role_update);
// Role.delete('/:id', [authJWT.verifyToken],  role_delete);
Role.post('/change-status',[authJWT.verifyToken], role_delete);

export default Role;