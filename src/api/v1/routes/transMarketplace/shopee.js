import express from 'express';
import authJWT from "../../middleware/authJWT.js";
import { 
    get_authorization_controller,
    save_authorization_controller,
    save_token_controller,
    get_shop_profile,
    get_category_list,
    get_product_list,
    delete_product_item,
    add_product_item,
    get_attributes_list,
    get_item_limit_list,
    get_item_base_info_list,
    get_item_extra_info_list,
    get_tier_variation_list,
    update_tier_variation_list,
    get_model_list,
    add_model_list,
    update_model_list,
    delete_model_list,
    update_price,
    update_stock,
    get_item_promotion,
    get_sip_item_price,
    search_item,
    category_recommend,
    get_order_list,
    get_shipment_list,
    get_order_detail,
    split_order,
    unsplit_order,
    cancel_order,
    handler_buyer_cancelation,
    add_invoice_data,
    get_return_list,
    get_return_detail,
    get_shipping_parameter,
    get_tracking_number,
    get_ship_order,
    update_shipping_order,
    get_shipping_document_parameter,
    create_shipping_document,
    get_shipping_document_result,
    get_tracking_info,
    get_address_list,
    get_channel_list,
    update_channel,
    batch_ship_order,
    get_unbind_order_list,
    get_detail_delivery,
    generate_first_mile_tracking_number,
    bind_first_mile_tracking_number,
    unbind_first_mile_tracking_number,
    get_tracking_number_list,
    get_waybill,
    get_channel_list_order
} from '../../controllers/Shopee.js';

const Shopee = express.Router();

Shopee.get('/shopee-auth',[authJWT.verifyToken], get_authorization_controller);
Shopee.post('/sync-shopee',[authJWT.verifyToken], save_authorization_controller);
Shopee.post('/store-token',[authJWT.verifyToken], save_token_controller);
Shopee.get('/get-shop-profile/:shop_id', [authJWT.verifyToken], get_shop_profile);
Shopee.get('/get-category-list/:shop_id',[authJWT.verifyToken], get_category_list);
Shopee.get('/get-attributes-list/:shop_id/:category_id',[authJWT.verifyToken], get_attributes_list);
Shopee.post('/get-product-list/:shop_id',[authJWT.verifyToken], get_product_list);
Shopee.get('/delete_product_item/:shop_id/:item_id',[authJWT.verifyToken], delete_product_item);
Shopee.post('/add_product_item/:shop_id',[authJWT.verifyToken], add_product_item);
Shopee.get('/get-item-limit-list/:shop_id/',[authJWT.verifyToken], get_item_limit_list);
Shopee.get('/get-item-base-info/:shop_id/:item_id',[authJWT.verifyToken], get_item_base_info_list);
Shopee.get('/get-item-extra-info/:shop_id/:item_id',[authJWT.verifyToken], get_item_extra_info_list);
Shopee.post('/tier-variation/:shop_id',[authJWT.verifyToken], get_tier_variation_list);
Shopee.post('/update-tier-variation/:shop_id',[authJWT.verifyToken], update_tier_variation_list);
Shopee.get('/get-model-list/:shop_id/:item_id',[authJWT.verifyToken], get_model_list);
Shopee.post('/add-model-list/:shop_id',[authJWT.verifyToken], add_model_list);
Shopee.post('/update-model-list/:shop_id',[authJWT.verifyToken], update_model_list);
Shopee.post('/delete-model-list/:shop_id',[authJWT.verifyToken], delete_model_list);
Shopee.post('/update-price/:shop_id',[authJWT.verifyToken], update_price);
Shopee.post('/update-stock/:shop_id',[authJWT.verifyToken], update_stock);
Shopee.get('/get-item-promotion/:shop_id/:item_id_list',[authJWT.verifyToken], get_item_promotion);
Shopee.post('/update-sip-item-price/:shop_id',[authJWT.verifyToken], get_sip_item_price);
Shopee.get('/search_item/:shop_id/:page_size/:item_name',[authJWT.verifyToken], search_item);
Shopee.get('/category_recommend/:shop_id/:item_name',[authJWT.verifyToken], category_recommend);
Shopee.post('/get_order_list/:shop_id',[authJWT.verifyToken], get_order_list);
Shopee.get('/get_shipment_list/:shop_id/:page_size',[authJWT.verifyToken], get_shipment_list);
Shopee.get('/get_order_detail/:shop_id/:order_sn_list',[authJWT.verifyToken], get_order_detail);
Shopee.post('/split_order/:shop_id',[authJWT.verifyToken], split_order);
Shopee.post('/unsplit_order/:shop_id',[authJWT.verifyToken], unsplit_order);
Shopee.post('/cancel_order/:shop_id',[authJWT.verifyToken], cancel_order);
Shopee.post('/handler_buyer_cancelation/:shop_id',[authJWT.verifyToken], handler_buyer_cancelation);
Shopee.post('/add_invoice_data/:shop_id',[authJWT.verifyToken], add_invoice_data);
Shopee.post('/get_return_list/:shop_id',[authJWT.verifyToken], get_return_list);
Shopee.get('/get_return_detail/:shop_id/:return_sn',[authJWT.verifyToken], get_return_detail);
Shopee.get('/get_shipping_parameter/:shop_id/:order_sn',[authJWT.verifyToken], get_shipping_parameter);
Shopee.post('/get_tracking_number/:shop_id',[authJWT.verifyToken], get_tracking_number);
Shopee.post('/get_ship_order/:shop_id',[authJWT.verifyToken], get_ship_order);
Shopee.post('/update_shipping_order/:shop_id',[authJWT.verifyToken], update_shipping_order);
Shopee.post('/get_shipping_document_parameter/:shop_id',[authJWT.verifyToken], get_shipping_document_parameter);
Shopee.post('/create_shipping_document/:shop_id',[authJWT.verifyToken], create_shipping_document);
Shopee.post('/get_shipping_document_result/:shop_id',[authJWT.verifyToken], get_shipping_document_result);
Shopee.get('/get_tracking_info/:shop_id/:order_sn/:package_number',[authJWT.verifyToken], get_tracking_info);
Shopee.get('/get_address_list/:shop_id',[authJWT.verifyToken], get_address_list);
Shopee.get('/get_channel_list/:shop_id',[authJWT.verifyToken], get_channel_list);
Shopee.post('/update_channel/:shop_id',[authJWT.verifyToken], update_channel);
Shopee.post('/batch_ship_order/:shop_id',[authJWT.verifyToken], batch_ship_order);
Shopee.post('/get_unbind_order_list/:shop_id',[authJWT.verifyToken], get_unbind_order_list);
Shopee.post('/get_detail_delivery/:shop_id',[authJWT.verifyToken], get_detail_delivery);
Shopee.post('/generate_first_mile_tracking_number/:shop_id',[authJWT.verifyToken], generate_first_mile_tracking_number);
Shopee.post('/bind_first_mile_tracking_number/:shop_id',[authJWT.verifyToken], bind_first_mile_tracking_number);
Shopee.post('/unbind_first_mile_tracking_number/:shop_id',[authJWT.verifyToken], unbind_first_mile_tracking_number);
Shopee.post('/get_tracking_number_list/:shop_id',[authJWT.verifyToken], get_tracking_number_list);
Shopee.post('/get_waybill/:shop_id',[authJWT.verifyToken], get_waybill);
Shopee.get('/get_channel_list_order/:shop_id/:region',[authJWT.verifyToken], get_channel_list_order);


export default Shopee;