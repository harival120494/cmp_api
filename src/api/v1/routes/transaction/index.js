import express from 'express';
import multer from "multer";
import getStream from 'get-stream'
import authJWT from "../../middleware/authJWT.js";
import { 
    get_list_toko,
    get_list_paket,
    detail_package_order,
    detail_invoice,
    update_profile_toko
} from '../../controllers/transaction.js';
import {ShopeeUploadImage} from '../../helper/ShopeeUploadImage.js'
import FormData from 'form-data'
import fs from 'fs'
import path from 'path'
/**
 * TRANSACTION  ROUTER
 * ==============================
 * Created by Harival Tivani
 * Created at January 25, 2022
 * ==============================
 */

let type_file = ''

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, 'public/uploads/shop_logo');
    },
    filename: async(req,  file, cb) => {
        const user_id = await authJWT.authData(req);
        const temp = file.mimetype.split('/');
        type_file = temp[1]
        // console.log(file)
        // cb(null, `${user_id}.${type_file}`);
        cb(null, file.originalname);

    }
});
const upload = multer({ storage });
const upload1 = multer()

const Transaction = express.Router();
/** Route by user */
Transaction.get('/list-toko',[authJWT.verifyToken], get_list_toko);
Transaction.get('/list-paket',[authJWT.verifyToken], get_list_paket);
Transaction.get('/detail-paket/:package_order_id',[authJWT.verifyToken], detail_package_order);
Transaction.get('/detail-invoice/:package_order_id',[authJWT.verifyToken], detail_invoice);
// Transaction.post('/update-profile-toko/:shop_id',[authJWT.verifyToken, upload1.single('shop_logo')], async function(req, res){
//     // update_profile_toko(req, res, type_file)
//     // return res.json(req.file)
//     const formData = new FormData();
//     formData.append('shop_logo', fs.createReadStream(path.dirname(req.file.originalname)));
//     const user_id = await authJWT.authData(req);
//     const upload = await ShopeeUploadImage(req.file, user_id, req.params.shop_id)
//     console.log(upload)
// });



export default Transaction;
