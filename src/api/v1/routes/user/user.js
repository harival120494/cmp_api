import express from 'express';
import {
    login,
    register,
    confirm_otp,
    reset_otp,
    all_user,
    user_role,
    register_byadmin,
    user_byid,
    update_user
} from'../../controllers/UserController.js'
import { registerValidator, registerByAdminValidator } from "../../middleware/validator.js";
import authJWT from "../../middleware/authJWT.js";

const User = express.Router();

User.post('/', login);
User.post('/register',[registerValidator()], register);
User.get('/confirm-otp/:id', confirm_otp);
User.get('/reset-otp/:id', reset_otp);
User.get('/user-list', [authJWT.verifyToken], all_user);
User.get('/user-role', [authJWT.verifyToken], user_role);
User.post('/register-byadmin',[authJWT.verifyToken, registerByAdminValidator()], register_byadmin);
User.get('/user-list/:id', [authJWT.verifyToken], user_byid);
User.put('/update/:id',[registerValidator()], update_user);

export default User;