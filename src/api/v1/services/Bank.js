import Bank from "../models/BankModel.js";
import moment from 'moment';

/**
 * BANK SERVICE
 * ==============================
 * Created by Harival Tivani
 * Created at December 30, 2021
 * ==============================
 */

export const list_bank_service = async () => {
    try {
        return ({status : 200 , data : await Bank.findAll({},{type : Bank.sequelize.QueryTypes.SELECT})});
    } catch (error) {
        return error;
    }
}

export const list_byid_service = async (id) => {
    try {
        return ({status : 200, data : await Bank.findOne({where : {bank_id : id}},{type : Bank.sequelize.QueryTypes.SELECT})});
    } catch (error) {
        return error;
    }
}

export const create_bank_service = async (req, user_id) => {
    try {
        return await Bank.create(
            { 
                bank_code : req.bank_code,
                bank_name : req.bank_name,
                bank_account_number : req.bank_account_number,
                bank_account_name : req.bank_account_name,
                status: req.status,
                created_by : user_id
            }
        )
        .then((result) => {
            return ({status : 201, data : result});
        })

    } catch (error) {
        return error;
    }
}

export const update_bank_service = async (req, bank_id, user_id) => {
    try {
        return await Bank.update(
            { 
                bank_code : req.bank_code,
                bank_name : req.bank_name,
                bank_account_number : req.bank_account_number,
                bank_account_name : req.bank_account_name,
                status: req.status,
                updated_by : user_id,
                updated_at : moment().format('YYYY-MM-DD HH:mm:ss')
            },
            {
                where: {
                    bank_id: bank_id
                },
                type : Bank.sequelize.QueryTypes.UPDATE
            }
        )
        .then((result) => {
            if(result == 1){return ({status : 200, message : 'Data updated successfully'})}
            else{return ({status : 400, message : 'Data update failed'})}
        })

    } catch (error) {
        return error;
    }
}

export const delete_bank_service = async (req, bank_id) => {
    try {
        return await Bank.update(
            { 
                status: req.status,
            },
            {
                where: {
                    bank_id: req.bank_id
                },
                type : Bank.sequelize.QueryTypes.UPDATE
            }
        )
        .then((result) => {
            if(result == 1){return ({status : 200, message : 'Data updated successfully'})}
            else{return ({status : 400, message : 'Data update failed'})}
        })
    } catch (error) {
        return error;
    }
}