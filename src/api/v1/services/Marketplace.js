import Marketplace from "../models/MarketplaceModel.js";
import moment from 'moment';
import { MarketplaceCodeGenerator } from "../helper/MarketplaceCode.js";
import { Sequelize } from 'sequelize';
/**
 * MARKETPLACE SERVICES
 * ==============================
 * Created by Harival Tivani
 * Created at December 27, 2021
 * ==============================
 */


export const marketplace_list_service = async (data) => {
    try {
        return Marketplace.findAll({})
            .then((results) => {
                if(results.length == 0){
                    return ({status : 204, data : "No content"});
                }
                else{
                    return ({status : 200, data : results});
                }
            })
    } catch (error) {
        return error;
    }
}


export const marketplace_create_service = async (req, user_id) => {
    try {
        const last_seq = await MarketplaceCodeGenerator();
        return Marketplace.create(
            { 
                marketplace_code : `MKP${last_seq}`,
                marketplace_name : req.marketplace_name,
                desc : req.desc,
                logo_url : `files/uploads/marketplace_logo/${req.fileName}`,
                status:req.status,
                created_user:user_id
            }
        )
        .then((results) => {
            return ({status:201, data : results});
        })
    } catch (error) {
        return error;
    }
}

export const marketplace_byid_service = async (id) => {
    try {
        return Marketplace.findOne({where : {marketplace_id : id}, type : Marketplace.sequelize.QueryTypes.SELECT})
            .then((results) => {
                if(results == null){
                    return ({status : 204, data : "No content"});
                }
                else{
                    return ({status : 200, data : results});
                }
            })
    } catch (error) {
        return error;
    }
}

export const marketplace_update_service = async (req, marketplace_id, user_id) => {
    try {
        return Marketplace.update({ 
                marketplace_code : req.marketplace_code,
                marketplace_name : req.marketplace_name,
                logo_url : `files/uploads/marketplace_logo/${req.fileName}`,
                desc : req.desc,
                status:req.status,
                updated_user:user_id,
                updated_date : moment().format('YYYY-MM-DD HH:mm:ss')
            }, 
            {
                where: {
                    marketplace_id: marketplace_id
                },
                type : Marketplace.sequelize.QueryTypes.UPDATE
            })
            .then(result => {
                if(result == 1){return ({status : 200, message : 'Data updated successfully'})}
                else{return ({status : 400, message : 'Data update failed'})}
            })
    } catch (error) {
        return error;
    }
}

export const marketplace_delete_service = async (marketplace_id, status, user_id) => {
    try {
        return Marketplace.update({ 
            status:status,
            updated_user:user_id,
            updated_date : moment().format('YYYY-MM-DD HH:mm:ss')
        }, 
        {
            where: {
                marketplace_id: marketplace_id
            },
            type : Marketplace.sequelize.QueryTypes.UPDATE
        })
        .then(result => {
            if(result == 1){return ({status : 200, message : 'Data deleted successfully'})}
            else{return ({status : 400, message : 'Data deleted failed'})}
        })
    } catch (error) {
        return error;
    }
}

export const marketplace_list_byuser_service = async (user_id, BASE_URL) => {
    try {
        const query = `SELECT 
                            cm.marketplace_name,
                            CONCAT('${BASE_URL}',cm.logo_url) as logo_url,
                            cm.marketplace_id
                        FROM tr_order_package as top
                        LEFT JOIN tr_detail_order_package as tdop ON top.order_package_id = tdop.order_package_id
                        LEFT JOIN d_package as dp ON dp.package_id = tdop.package_id
                        LEFT JOIN c_marketplace as cm ON cm.marketplace_id = dp.marketplace_id
                        WHERE top.user_id=$1 AND top.status_id=$2
                        GROUP by cm.marketplace_id`;

        return await Marketplace.sequelize.query(query, { bind: [user_id, 6], type: Sequelize.QueryTypes.SELECT})
            .then( async (results) => {
                if(results.length > 0){
                    return ({status : 200, data : results});
                }
                else {
                    return ({status : 204, data : "No content"});
                }
            });
    } catch (error) {
        return error;
    }
}