import OrderPackage from "../models/OrderPaket.js";
import OrderPackageDetail from "../models/OrderPaketDetail.js";
import { InvoiceGenerator } from "../helper/InvoiceNoGenerator.js";
import { Sequelize } from 'sequelize';
import moment from 'moment';
import dotenv from "dotenv";
dotenv.config();


const Op = Sequelize.Op;


function detail_order(id){
    return OrderPackageDetail.sequelize.query(`SELECT
                tdop.harga_satuan,
                tdop.qty,
                (tdop.harga_satuan * tdop.qty) as total_harga,
                tdop.detail_order_package_id,
                tdop.package_id,
                dp.package_name,
                dp.desc,
                dp.marketplace_id,
                cm.marketplace_name
            FROM tr_detail_order_package as tdop
            LEFT JOIN d_package as dp ON tdop.package_id = dp.package_id
            LEFT JOIN c_marketplace as cm ON cm.marketplace_id = dp.marketplace_id
            WHERE tdop.order_package_id = $1 `,
        { bind: [id],type: Sequelize.QueryTypes.SELECT }
    ).then( async (result) => {
        return result
    });
}


/**
 * ORDER PACKAGE SERVICES
 * ==============================
 * Created by Harival Tivani
 * Created at Januari 01, 2022
 * ==============================
 */


export const create_order_paket_service = async (data, user_id) => {
    try {
        const no_invoice = await InvoiceGenerator(user_id);
        /**
         * Getting store base price
         */
        const base_price = await OrderPackage.sequelize.query(`SELECT * FROM c_harga_toko WHERE status = $1 `,{ bind: [1],type: Sequelize.QueryTypes.SELECT });
        
        let detail_result = [];
        const params = {
            user_id : user_id,
            status_id : 4,
            no_invoice : no_invoice,
            jumlah_toko : data.jumlah_toko,
            harga_pertoko : base_price[0].harga_toko_price,
            create_user : user_id
        }
        return await OrderPackage.create(params)
            .then((results) => {
                if(results){
                    for (let item of data.detail) {
                        const detail = {
                            package_id : item.package_id,
                            order_package_id : results.order_package_id,
                            harga_satuan:item.harga_satuan,
                            qty : item.qty,
                            status : 0,
                            create_user : user_id
                        }
                        OrderPackageDetail.create(detail).then((results1) => { 
                            detail_result.push(results1)
                        })
                    }
                    return ({status:201, data : results});
                }
                else{
                    return ({status:400, data : "Data gagal disimpan"});
                }
            })
    } catch (error) {
        return error
    }   
}

export const pembayaran_service = async (id, user_id) => {
    const query = {
        attributes : [
            'user_id',
            'order_package_id',
            'jumlah_toko',
            'harga_pertoko',
            [Sequelize.literal(`(harga_pertoko * jumlah_toko)`), 'total_harga_toko'],
            'no_invoice'
        ],
        where: {
            [Op.and]: [
              { user_id: user_id },
              { status_id: 4 },
              {order_package_id : id}
            ]
        },
        type : OrderPackage.sequelize.QueryTypes.SELECT
    }
    return await OrderPackage.findOne(query)
            .then( async (results) => {
                if(results != null){
                    return ({status : 200, data_header : results, data_detail : await detail_order(results.order_package_id)});
                }
                else{
                    return ({status : 204, data_header : "No content", data_detail : "No content"});
                }
            })
}

export const upload_bukti_pembayaran_service = async (data, id_order, user_id) => {
    try {
        const d = new Date();
        const format = `${d.getYear()}${d.getMonth() + 1}${d.getDate()}${d.getHours()}${d.getMinutes()}`;
        const fileName = format+''+data.file.originalname
        const data_body = data.body;
        const param = {
            bank_id : data_body.bank_id,
            image_url : `files/uploads/${fileName}` ,
            tgl_pembayaran : data_body.tgl_bayar,
            tgl_upload_bukti : moment().format('YYYY-MM-DD HH:mm:ss'),
            status_id : 5
        }

        return await OrderPackage.update(param, {
                where : {
                    order_package_id : id_order
                }
            })
            .then((results) => {
                if(results[0]){
                    return ({status : 200, message : "Bukti pembayaran berhasil di upload"})
                }
                else{
                    return ({status : 400, message : "Bukti pembayaran gagal di upload"})
                }
            })
    } catch (error) {
        return error;
    }
}

export const list_pembayaran_service = async () => {
    try {
        const query = `SELECT 
                        top.order_package_id,
                        top.no_invoice,
                        u.email,
                        u.nohp,
                        top.tgl_pembayaran,
                        top.tgl_upload_bukti,
                        top.jumlah_toko,
                        top.harga_pertoko,
                        (top.jumlah_toko * top.harga_pertoko) as ttl_hrg_toko,
                        detail.total_detail,
                        (detail.total_detail + (top.jumlah_toko * top.harga_pertoko)) as grand_total,
                        cas.status_name	
                    FROM tr_order_package as top
                    LEFT JOIN users as u ON u.user_id = top.user_id
                    LEFT JOIN c_all_status as cas ON cas.status_id = top.status_id
                    LEFT JOIN (
                        SELECT 
                            tdop.order_package_id,
                            SUM(tdop.harga_satuan) as total_detail
                        FROM tr_detail_order_package as tdop
                    ) as detail 
                    ON detail.order_package_id = top.order_package_id
                    GROUP BY top.no_invoice`;
        return ({status : 200, data : await OrderPackage.sequelize.query(query, {type: Sequelize.QueryTypes.SELECT})});
    } catch (error) {
        return error;
    }
}

export const detail_list_pembayaran_service = async(id_order) => {
    try {
        const query = `SELECT 
                            top.order_package_id,
                            top.no_invoice,
                            u.email,
                            u.nohp,
                            cb.bank_name,
                            top.tgl_pembayaran,	
                            top.image_url,
                            (detail.total_detail + (top.jumlah_toko * top.harga_pertoko)) as grand_total
                        FROM tr_order_package as top
                        LEFT JOIN users as u ON u.user_id = top.user_id
                        LEFT JOIN (
                            SELECT 
                                tdop.order_package_id,
                                SUM(tdop.harga_satuan) as total_detail
                            FROM tr_detail_order_package as tdop
                        ) as detail 
                        ON detail.order_package_id = top.order_package_id
                        LEFT JOIN c_bank as cb ON cb.bank_id = top.bank_id
                        WHERE top.order_package_id = $1`;

        return await OrderPackage.sequelize.query(query, { bind: [id_order], type: Sequelize.QueryTypes.SELECT })
                .then( async (results) => {
                    if(results.length > 0){
                        return ({status : 200, data_header : results, data_detail : await detail_order(id_order)});
                    }
                    else {
                        return ({status : 204, data_header : "No content", data_detail : "No content"});
                    }
                });
    } catch (error) {
        return error
    }
}

export const accept_pembayaran_service = async (data, user_id) => {
    try {
        
        const param = {
            status_id : data.status,
            approve_by : user_id,
            approve_date : moment().format('YYYY-MM-DD HH:mm:ss'),
            update_user : user_id,
            update_date : moment().format('YYYY-MM-DD HH:mm:ss')
        }
        
        return await OrderPackage.update(param, {
                where : {
                    order_package_id : data.id_order
                }
            })
            .then(async (results) => {
                if(results[0] > 0){
                    const sub_query = `UPDATE tr_detail_order_package as b
                                        JOIN (
                                            SELECT 
                                                    tdop.package_id,
                                                    tdop.detail_order_package_id,
                                                    DATE_ADD(DATE(NOW()), INTERVAL cp.jumlah_hari DAY) as exp_date
                                                FROM tr_detail_order_package as tdop
                                                LEFT JOIN d_package as dp ON dp.package_id = tdop.package_id
                                                LEFT JOIN c_periode as cp ON cp.periode_id = dp.periode_id
                                                WHERE tdop.order_package_id = $1
                                        ) as a on b.detail_order_package_id = a.detail_order_package_id
                                        set b.exp_date = a.exp_date
                                        WHERE b.order_package_id = $1`;

                    return await OrderPackageDetail.sequelize.query(sub_query, { bind: [data.id_order], type: Sequelize.QueryTypes.UPDATE })
                    .then((results) => {
                        if(results[1] > 0){return ({status : 200, message : "Data berhasil diupdate"})}
                        else{return ({status : 400, message : "Data gagal diupdate"})}
                    })
                }
                else{
                    return ({status : 400, message : "Data gagal diupdate"})
                }
            })
    } catch (error) {
        return error;
    }
}