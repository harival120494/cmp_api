import Package from "../models/PackageModel.js";
import moment from 'moment';
import { PackageCodeGenerator } from "../helper/PackageCode.js";
import { Sequelize } from 'sequelize';

/**
 * PACKAGE SERVICES
 * ==============================
 * Created by Harival Tivani
 * Created at December 27, 2021
 * ==============================
 */



export const package_list_service = async (BASE_URL) => {
    try {
        const now = moment().format('YYYY-MM-DD');
        
        return Package.sequelize.query(`SELECT 
                            p.package_id,
                            p.package_code,
                            p.package_name,
                            p.start_date,
                            p.end_date,
                            p.price,
                            p.status,
                            p.created_date,
                            CONCAT(1,'') as qty,
                            (p.price * 1) as harga_total,
                            p.desc as paket_desc,
                            m.marketplace_id,
                            m.marketplace_code,
                            CONCAT('${BASE_URL}',m.logo_url) as logo_url,
                            m.marketplace_name,
                            m.desc as marketplace_desc,
                            periode.periode_id,
                            periode.periode_name,
                            periode.jumlah_hari
                        FROM d_package as p
                        LEFT JOIN c_marketplace as m ON p.marketplace_id = m.marketplace_id
                        LEFT JOIN c_periode as periode ON p.periode_id = periode.periode_id
                        ORDER BY p.start_date DESC `,
            { type: Sequelize.QueryTypes.SELECT }
        ).then(function(result) {
            return Package.sequelize.query(`select * from c_harga_toko where status=$1`,
                    { bind: [1],type: Sequelize.QueryTypes.SELECT }
                )
                .then(function(result1) {
                    return({status : 200, header : result1, data : result});
                })
            // return({status : 200, data : result});
        });
    } catch (error) {
        return error;
    }
}

export const package_create_service = async (req, user_id) => {
    try {
        const last_seq = await PackageCodeGenerator();
        return Package.create(
            { 
                marketplace_id :req.marketplace_id,
                periode_id : req.periode_id,
                package_code : `PKG${last_seq}`,
                package_name : req.package_name,
                price:req.price,
                start_date : req.start_date,
                end_date : req.end_date,
                desc : req.desc,
                status:req.status,
                create_user:user_id
            }
        )
        .then((results) => {
            return ({status:201, data : results});
        })
    } catch (error) {
        return error;
    }
}

export const package_byid_service = async (id) => {
    try {
        const now = moment().format('YYYY-MM-DD');
        return Package.sequelize.query(`SELECT 
                            p.package_id,
                            p.package_code,
                            p.package_name,
                            p.start_date,
                            p.end_date,
                            p.price,
                            p.status,
                            p.created_date,
                            CONCAT(1,'') as qty,
                            (p.price * 1) as harga_total,
                            p.desc as paket_desc,
                            m.marketplace_code,
                            m.marketplace_name,
                            m.desc as marketplace_desc,
                            periode.periode_name,
                            periode.jumlah_hari
                        FROM d_package as p
                        LEFT JOIN c_marketplace as m ON p.marketplace_id = m.marketplace_id
                        LEFT JOIN c_periode as periode ON p.periode_id = periode.periode_id
                        WHERE p.package_id = $1
                        ORDER BY p.start_date DESC `,
            {bind: [id], type: Sequelize.QueryTypes.SELECT }
        ).then(function(result) {
            return({status : 200, data : result});
        });
    } catch (error) {
        return error;
    }
}

export const package_update_service = async (req, package_id, user_id) => {
    try {
        return Package.update({ 
                marketplace_id :req.marketplace_id,
                periode_id : req.periode_id,
                package_code : req.package_code,
                package_name : req.package_name,
                price:req.price,
                start_date : req.start_date,
                end_date : req.end_date,
                desc : req.desc,
                status:req.status,
                update_user:user_id,
                updated_date : moment().format('YYYY-MM-DD HH:mm:ss')
            }, 
            {
                where: {
                    package_id: package_id
                },
                type : Package.sequelize.QueryTypes.update
            })
            .then(result => {
                if(result == 1){return ({status : 200, message : 'Data updated successfully'})}
                else{return ({status : 400, message : 'Data update failed'})}
            })
    } catch (error) {
        return error;
    }
}

export const package_delete_service = async (package_id, status, user_id) => {
    try {
        return Package.update({ 
            status:status,
            update_user:user_id,
            updated_date : moment().format('YYYY-MM-DD HH:mm:ss')
        }, 
        {
            where: {
                package_id: package_id
            },
            type : Package.sequelize.QueryTypes.UPDATE
        })
        .then(result => {
            if(result == 1){return ({status : 200, message : 'Data updated successfully'})}
            else{return ({status : 400, message : 'Data updated failed'})}
        })
    } catch (error) {
        return error;
    }
}

/**
 * Package service accessing by user
 * ==============================
 * Created by Harival Tivani
 * Created at December 30, 2021
 * ==============================
 */
export const package_list_user_service = async (data) => {
    try {
        const now = moment().format('YYYY-MM-DD');
        return Package.sequelize.query(`SELECT 
                            p.package_id,
                            p.package_code,
                            p.package_name,
                            p.start_date,
                            p.end_date,
                            p.price,
                            p.status,
                            p.created_date,
                            CONCAT(1,'') as qty,
                            (p.price * 1) as harga_total,
                            p.desc as paket_desc,
                            m.marketplace_code,
                            m.marketplace_name,
                            m.desc as marketplace_desc,
                            periode.periode_name,
                            periode.jumlah_hari
                        FROM d_package as p
                        LEFT JOIN c_marketplace as m ON p.marketplace_id = m.marketplace_id
                        LEFT JOIN c_periode as periode ON p.periode_id = periode.periode_id
                        WHERE (p.start_date <= DATE(NOW()) AND p.end_date >= DATE(NOW())) AND p.status = $1
                        ORDER BY p.start_date DESC `,
            { bind: [1],type: Sequelize.QueryTypes.SELECT }
        ).then(function(result) {
            return({status : 200, data : result});
        });
    } catch (error) {
        return error;
    }
}
