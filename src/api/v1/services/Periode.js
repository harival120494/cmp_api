import Periode from "../models/PeriodeModel.js";
import moment from 'moment';

/**
 * PERIODE SERVICES
 * ==============================
 * Created by Harival Tivani
 * Created at December 27, 2021
 * ==============================
 */


export const periode_list_service = async (data) => {
    try {
        return Periode.findAll({})
            .then((results) => {
                if(results.length == 0){
                    return ({status : 204, data : "No content"});
                }
                else{
                    return ({status : 200, data : results});
                }
            })
    } catch (error) {
        return error;
    }
}


export const periode_create_service = async (req, user_id) => {
    try {
        return Periode.create(
            { 
                periode_name : req.periode_name,
                jumlah_hari : req.jumlah_hari,
                status:req.status,
                created_user:user_id
            }
        )
        .then((results) => {
            return ({status:201, data : results});
        })
    } catch (error) {
        return error;
    }
}

export const periode_byid_service = async (id) => {
    try {
        return Periode.findOne({where : {periode_id : id}, type : Periode.sequelize.QueryTypes.SELECT})
            .then((results) => {
                if(results == null){
                    return ({status : 204, data : "No content"});
                }
                else{
                    return ({status : 200, data : results});
                }
            })
    } catch (error) {
        return error;
    }
}

export const periode_update_service = async (req, periode_id, user_id) => {
    try {
        return Periode.update({ 
                periode_name : req.periode_name,
                jumlah_hari : req.jumlah_hari,
                status:req.status,
                updated_user:user_id,
                updated_date : moment().format('YYYY-MM-DD HH:mm:ss')
            }, 
            {
                where: {
                    periode_id: periode_id
                },
                type : Periode.sequelize.QueryTypes.UPDATE
            })
            .then(result => {
                if(result == 1){return ({status : 200, message : 'Data updated successfully'})}
                else{return ({status : 400, message : 'Data update failed'})}
            })
    } catch (error) {
        return error;
    }
}

export const periode_delete_service = async (periode_id, user_id) => {
    try {
        return Periode.update({ 
            status:0,
            updated_user:user_id,
            updated_date : moment().format('YYYY-MM-DD HH:mm:ss')
        }, 
        {
            where: {
                periode_id: periode_id
            },
            type : Periode.sequelize.QueryTypes.UPDATE
        })
        .then(result => {
            if(result == 1){return ({status : 200, message : 'Data deleted successfully'})}
            else{return ({status : 400, message : 'Data deleted failed'})}
        })
    } catch (error) {
        return error;
    }
}

export const periode_cs_service = async (periode_id, status, user_id) => {
    try {
        return Periode.update({ 
            status:status,
            updated_user : user_id,
            updated_date : moment().format('YYYY-MM-DD HH:mm:ss')
        }, 
        {
            where: {
                periode_id: periode_id
            },
            type : Periode.sequelize.QueryTypes.UPDATE
        })
        .then(result => {
            if(result == 1){return ({status : 200, message : 'Periode status berhasil dirubah'})}
            else{return ({status : 400, message : 'Periode status gagal dirubah'})}
        })
    } catch (error) {
        return error;
    }
}