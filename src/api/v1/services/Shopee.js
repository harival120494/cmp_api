import bcrypt from "bcrypt";
import dotenv from "dotenv";
import moment from 'moment';
import DShop from "../models/DShopModel.js";
import { Sequelize } from 'sequelize';
import ShopeeAPI from "../../../../const/Shopee.js";
import CryptoJS from "crypto-js";
import axios from 'axios';
import {AccessToken} from '../helper/AccessToken.js'
import {ShopeeGetShopProfile} from '../helper/ShopeeGetShopProfile.js'
dotenv.config();

/**
 * SHOPEE SERVICES
 * ==============================
 * Created by Harival Tivani
 * Created at January 21, 2022
 * ==============================
 */

// const timestamp = Math.floor(new Date().valueOf() / 1000)
const timestamp = moment().unix()
const partner_id = Number(ShopeeAPI.SHOPEE_PARTNER_ID)
const partner_key = ShopeeAPI.SHOPEE_PARTNER_KEY
const host = ShopeeAPI.SHOPEE_AUTH_HOST

export const get_authorization_shopee = async () => {

    const host = ShopeeAPI.SHOPEE_AUTH_HOST
    const path = ShopeeAPI.SHOPEE_AUTH_PATH
    const redirect = ShopeeAPI.REDIRECT_URL
    const sign_string = `${partner_id}${path}${Number(timestamp)}`
    const sign = CryptoJS.HmacSHA256(sign_string, partner_key).toString()
    const api = `${host}${path}?partner_id=${partner_id}&redirect=${redirect}&timestamp=${Number(timestamp)}&sign=${sign}`;
    return ({status : 200, data : api})
   
} 

export const store_authorization_shopee = async (data, user_id) => {
    try {
        const findOne = await DShop.findOne({where : {shop_id_marketplace : data.shop_id_marketplace}},{type : DShop.sequelize.QueryTypes.SELECT});
        if(findOne)
        {    
            return await DShop.update(
                { 
                    code_marketplace : data.code_marketplace, 
                    marketplace_id : data.marketplace_id, 
                    update_user : user_id
                }, 
                {
                    where: { shop_id_marketplace : data.shop_id_marketplace },
                    type : DShop.sequelize.QueryTypes.update
                })
                .then(result => {
                    if(result){return ({status : 200, data : findOne})}
                    else{return ({status : 400, data : 'Data update failed'})}
                })
        }
        else
        {
            return await DShop.create(
                { 
                    user_id : user_id,
                    marketplace_id : data.marketplace_id,
                    code_marketplace : data.code_marketplace,
                    shop_id_marketplace : data.shop_id_marketplace,
                    create_user : user_id
                }
            )
            .then((result) => {
                return({status : 200, data : result});
            })
        }
    } catch (error) {
        return error
    }
}

export const store_token_shopee = async (data, user_id) => {
    try {
        
        const path          = ShopeeAPI.SHOPEE_AUTH_TOKEN_URL
        const baseString    = `${partner_id}${path}${timestamp}`
        const sign          = CryptoJS.HmacSHA256(baseString, partner_key).toString()
        const api           = `${host}${path}?partner_id=${partner_id}&timestamp=${timestamp}&sign=${sign}`;
        const header        = {headers : {"Content-Type": "application/json"}}
        const body = {
            code : data.code,
            shop_id : Number(data.shop_id),
            partner_id : partner_id
        }
        
        return await axios.post(api, body, header).then(async(result) => {
            console.log(result)
            if(result){
                return await DShop.update(
                    { 
                        request_id      : result.data.request_id,
                        access_token    : result.data.access_token, 
                        refresh_token   : result.data.refresh_token, 
                        expire_in       : result.data.expire_in,
                        updated_date    : moment().format('YYYY-MM-DD HH:mm:ss')
                    }, 
                    {
                        where: { shop_id_marketplace : data.shop_id },
                        type : DShop.sequelize.QueryTypes.update
                    })
                    .then(async (result_update) => {
                        if(result_update){
                            const base_string   = `${partner_id}${ShopeeAPI.SHOPEE_GET_SHOP_PROFILE}${Number(timestamp)}${result.data.access_token}${Number(data.shop_id)}`;
                            const sign          = CryptoJS.HmacSHA256(base_string, partner_key).toString()
                            const data_shop = {
                                shop_id         : Number(data.shop_id),
                                partner_id      : partner_id,
                                access_token    : result.data.access_token,
                                sign            : sign,
                                timestamp       : timestamp
                            }
                            return await ShopeeGetShopProfile(data_shop)          
                        }          
                    })
            }
        })

    } catch (error) {
        return error
    }
}

export const get_shop_profile_service = async (user_id, shop_id) => {
    const access_token = await AccessToken(user_id, shop_id)
    let base_string = `${partner_id}${ShopeeAPI.SHOPEE_GET_SHOP_PROFILE}${Number(timestamp)}${access_token.token}${Number(shop_id)}`;
    let sign        = CryptoJS.HmacSHA256(base_string, partner_key).toString()
    const data_shop = {
        shop_id         : Number(shop_id),
        partner_id      : partner_id,
        access_token    : access_token.token,
        sign            : sign,
        timestamp       : timestamp
    }
    return await ShopeeGetShopProfile(data_shop)
}

export const get_category_list_service = async (shop_id, user_id) => {
    const access_token = await AccessToken(user_id, shop_id)
    let base_string = `${partner_id}${ShopeeAPI.GET_CATEGORY_LIST}${Number(timestamp)}${access_token.token}${Number(shop_id)}`;
    let sign        = CryptoJS.HmacSHA256(base_string, partner_key).toString()
    const header    = {headers : {"Content-Type": "application/json"}}
    const api       = `${host}${ShopeeAPI.GET_CATEGORY_LIST}?language=id&shop_id=${Number(shop_id)}&partner_id=${Number(partner_id)}&access_token=${access_token.token}&sign=${sign}&timestamp=${timestamp}`
   return await axios.get(api, header).then(async (result) => {
        return result.data
    })
}

export const get_attributes_list_service = async (shop_id, category_id, user_id) => {
    const access_token = await AccessToken(user_id, shop_id)
    let base_string = `${partner_id}${ShopeeAPI.GET_ATTRIBUTES_LIST}${Number(timestamp)}${access_token.token}${Number(shop_id)}`;
    let sign        = CryptoJS.HmacSHA256(base_string, partner_key).toString()
    const header    = {headers : {"Content-Type": "application/json"}}
    const api       = `${host}${ShopeeAPI.GET_ATTRIBUTES_LIST}?category_id=${category_id}&shop_id=${Number(shop_id)}&partner_id=${Number(partner_id)}&access_token=${access_token.token}&sign=${sign}&timestamp=${timestamp}`
   return await axios.get(api, header).then(async (result) => {
        return result.data
    })
}

export const get_item_limit_service = async (shop_id,  user_id) => {
    const access_token = await AccessToken(user_id, shop_id)
    let base_string = `${partner_id}${ShopeeAPI.GET_ITEM_LIMIT}${Number(timestamp)}${access_token.token}${Number(shop_id)}`;
    let sign        = CryptoJS.HmacSHA256(base_string, partner_key).toString()
    const header    = {headers : {"Content-Type": "application/json"}}
    const api       = `${host}${ShopeeAPI.GET_ITEM_LIMIT}?shop_id=${Number(shop_id)}&partner_id=${Number(partner_id)}&access_token=${access_token.token}&sign=${sign}&timestamp=${timestamp}`
   return await axios.get(api, header).then(async (result) => {
        return result.data
    })
}

export const get_product_list_service = async (shop_id, data, update_time_from, update_time_to, user_id) => {
    const access_token = await AccessToken(user_id, shop_id)
    let base_string = `${partner_id}${ShopeeAPI.PRODUCT_LIST}${Number(timestamp)}${access_token.token}${Number(shop_id)}`;
    let sign        = CryptoJS.HmacSHA256(base_string, partner_key).toString()
    const header    = {headers : {"Content-Type": "application/json"}}
    const params    = `offset=${data.offset}&page_size=${data.page_size}&update_time_from=${update_time_from}&update_time_to=${update_time_to}&item_status=${data.item_status}`
    const api       = `${host}${ShopeeAPI.PRODUCT_LIST}?${params}&shop_id=${Number(shop_id)}&partner_id=${Number(partner_id)}&access_token=${access_token.token}&sign=${sign}&timestamp=${timestamp}`
    // return ({data : moment.tz.names()})
    return await axios.get(api, header).then(async (result) => {
        return result.data
    })
}

export const delete_product_item_service = async (shop_id, item_id, user_id) => {
    const access_token = await AccessToken(user_id, shop_id)
    let base_string = `${partner_id}${ShopeeAPI.DELETE_ITEM}${Number(timestamp)}${access_token.token}${Number(shop_id)}`;
    let sign        = CryptoJS.HmacSHA256(base_string, partner_key).toString()
    const header    = {headers : {"Content-Type": "application/json"}}
    const api       = `${host}${ShopeeAPI.DELETE_ITEM}?shop_id=${Number(shop_id)}&partner_id=${Number(partner_id)}&access_token=${access_token.token}&sign=${sign}&timestamp=${timestamp}`
    const body = {item_id : Number(item_id) }
   
    return await axios.post(api, body, header).then(async (result) => {
        return result.data
    })
}

export const add_product_item_service = async (shop_id, data, user_id) => {
    const access_token = await AccessToken(user_id, shop_id)
    let base_string = `${partner_id}${ShopeeAPI.ADD_PRODUCT}${Number(timestamp)}${access_token.token}${Number(shop_id)}`;
    let sign        = CryptoJS.HmacSHA256(base_string, partner_key).toString()
    const header    = {headers : {"Content-Type": "application/json"}}
    const api       = `${host}${ShopeeAPI.ADD_PRODUCT}?shop_id=${Number(shop_id)}&partner_id=${Number(partner_id)}&access_token=${access_token.token}&sign=${sign}&timestamp=${timestamp}`
   
    return await axios.post(api, data, header).then(async (result) => {
        return result.data
    })
}

export const get_item_base_info_service = async (shop_id, item_id, user_id) => {
    const access_token = await AccessToken(user_id, shop_id)
    let base_string = `${partner_id}${ShopeeAPI.GET_ITEM_BASE_INFO}${Number(timestamp)}${access_token.token}${Number(shop_id)}`;
    let sign        = CryptoJS.HmacSHA256(base_string, partner_key).toString()
    const header    = {headers : {"Content-Type": "application/json"}}
    const api       = `${host}${ShopeeAPI.GET_ITEM_BASE_INFO}?item_id_list=${item_id}&language=id&shop_id=${Number(shop_id)}&partner_id=${Number(partner_id)}&access_token=${access_token.token}&sign=${sign}&timestamp=${timestamp}`
   return await axios.get(api, header).then(async (result) => {
        return result.data
    })
}

export const get_item_extra_info_service = async (shop_id, item_id, user_id) => {
    const access_token = await AccessToken(user_id, shop_id)
    let base_string = `${partner_id}${ShopeeAPI.GET_ITEM_EXTRA_INFO}${Number(timestamp)}${access_token.token}${Number(shop_id)}`;
    let sign        = CryptoJS.HmacSHA256(base_string, partner_key).toString()
    const header    = {headers : {"Content-Type": "application/json"}}
    const api       = `${host}${ShopeeAPI.GET_ITEM_EXTRA_INFO}?item_id_list=${item_id}&language=id&shop_id=${Number(shop_id)}&partner_id=${Number(partner_id)}&access_token=${access_token.token}&sign=${sign}&timestamp=${timestamp}`
   return await axios.get(api, header).then(async (result) => {
        return result.data
    })
}

export const get_tier_variation_service = async (shop_id, data, user_id) => {
    const access_token = await AccessToken(user_id, shop_id)
    let base_string = `${partner_id}${ShopeeAPI.INIT_TIER_VARIATION}${Number(timestamp)}${access_token.token}${Number(shop_id)}`;
    let sign        = CryptoJS.HmacSHA256(base_string, partner_key).toString()
    const header    = {headers : {"Content-Type": "application/json"}}
    const api       = `${host}${ShopeeAPI.INIT_TIER_VARIATION}?&shop_id=${Number(shop_id)}&partner_id=${Number(partner_id)}&access_token=${access_token.token}&sign=${sign}&timestamp=${timestamp}`
    
    return await axios.post(api, data, header).then(async (result) => {
        return result.data
    })
}

export const update_tier_variation_service = async (shop_id, data, user_id) => {
    const access_token = await AccessToken(user_id, shop_id)
    let base_string = `${partner_id}${ShopeeAPI.UPDATE_TIER_VARIATION}${Number(timestamp)}${access_token.token}${Number(shop_id)}`;
    let sign        = CryptoJS.HmacSHA256(base_string, partner_key).toString()
    const header    = {headers : {"Content-Type": "application/json"}}
    const api       = `${host}${ShopeeAPI.UPDATE_TIER_VARIATION}?&shop_id=${Number(shop_id)}&partner_id=${Number(partner_id)}&access_token=${access_token.token}&sign=${sign}&timestamp=${timestamp}`
    
    return await axios.post(api, data, header).then(async (result) => {
        return result.data
    })
}

export const get_model_list_service = async (shop_id, item_id, user_id) => {
    const access_token = await AccessToken(user_id, shop_id)
    let base_string = `${partner_id}${ShopeeAPI.GET_MODEL_LIST}${Number(timestamp)}${access_token.token}${Number(shop_id)}`;
    let sign        = CryptoJS.HmacSHA256(base_string, partner_key).toString()
    const header    = {headers : {"Content-Type": "application/json"}}
    const api       = `${host}${ShopeeAPI.GET_MODEL_LIST}?item_id=${item_id}&shop_id=${Number(shop_id)}&partner_id=${Number(partner_id)}&access_token=${access_token.token}&sign=${sign}&timestamp=${timestamp}`
    
    return await axios.get(api, header).then(async (result) => {
        return result.data
    })
}

export const add_model_list_service = async (shop_id, data, user_id) => {
    const access_token = await AccessToken(user_id, shop_id)
    let base_string = `${partner_id}${ShopeeAPI.ADD_MODEL_LIST}${Number(timestamp)}${access_token.token}${Number(shop_id)}`;
    let sign        = CryptoJS.HmacSHA256(base_string, partner_key).toString()
    const header    = {headers : {"Content-Type": "application/json"}}
    const api       = `${host}${ShopeeAPI.ADD_MODEL_LIST}?shop_id=${Number(shop_id)}&partner_id=${Number(partner_id)}&access_token=${access_token.token}&sign=${sign}&timestamp=${timestamp}`
    
    return await axios.post(api, data, header).then(async (result) => {
        return result.data
    })
}

export const update_model_list_service = async (shop_id, data, user_id) => {
    const access_token = await AccessToken(user_id, shop_id)
    let base_string = `${partner_id}${ShopeeAPI.UPDATE_MODEL_LIST}${Number(timestamp)}${access_token.token}${Number(shop_id)}`;
    let sign        = CryptoJS.HmacSHA256(base_string, partner_key).toString()
    const header    = {headers : {"Content-Type": "application/json"}}
    const api       = `${host}${ShopeeAPI.UPDATE_MODEL_LIST}?shop_id=${Number(shop_id)}&partner_id=${Number(partner_id)}&access_token=${access_token.token}&sign=${sign}&timestamp=${timestamp}`
    
    return await axios.post(api, data, header).then(async (result) => {
        return result.data
    })
}

export const delete_model_list_service = async (shop_id, data, user_id) => {
    const access_token = await AccessToken(user_id, shop_id)
    let base_string = `${partner_id}${ShopeeAPI.DELETE_MODEL_LIST}${Number(timestamp)}${access_token.token}${Number(shop_id)}`;
    let sign        = CryptoJS.HmacSHA256(base_string, partner_key).toString()
    const header    = {headers : {"Content-Type": "application/json"}}
    const api       = `${host}${ShopeeAPI.DELETE_MODEL_LIST}?shop_id=${Number(shop_id)}&partner_id=${Number(partner_id)}&access_token=${access_token.token}&sign=${sign}&timestamp=${timestamp}`
    
    return await axios.post(api, data, header).then(async (result) => {
        return result.data
    })
}

export const update_price_service = async (shop_id, data, user_id) => {
    const access_token = await AccessToken(user_id, shop_id)
    let base_string = `${partner_id}${ShopeeAPI.UPDATE_PRICE}${Number(timestamp)}${access_token.token}${Number(shop_id)}`;
    let sign        = CryptoJS.HmacSHA256(base_string, partner_key).toString()
    const header    = {headers : {"Content-Type": "application/json"}}
    const api       = `${host}${ShopeeAPI.UPDATE_PRICE}?shop_id=${Number(shop_id)}&partner_id=${Number(partner_id)}&access_token=${access_token.token}&sign=${sign}&timestamp=${timestamp}`
    
    return await axios.post(api, data, header).then(async (result) => {
        return result.data
    })
}

export const update_stock_service = async (shop_id, data, user_id) => {
    const access_token = await AccessToken(user_id, shop_id)
    let base_string = `${partner_id}${ShopeeAPI.UPDATE_STOCK}${Number(timestamp)}${access_token.token}${Number(shop_id)}`;
    let sign        = CryptoJS.HmacSHA256(base_string, partner_key).toString()
    const header    = {headers : {"Content-Type": "application/json"}}
    const api       = `${host}${ShopeeAPI.UPDATE_STOCK}?shop_id=${Number(shop_id)}&partner_id=${Number(partner_id)}&access_token=${access_token.token}&sign=${sign}&timestamp=${timestamp}`
    
    return await axios.post(api, data, header).then(async (result) => {
        return result.data
    })
}

export const get_item_promotion_service = async (shop_id, item_id_list, user_id) => {
    const access_token = await AccessToken(user_id, shop_id)
    let base_string = `${partner_id}${ShopeeAPI.GET_ITEM_PROMOTION}${Number(timestamp)}${access_token.token}${Number(shop_id)}`;
    let sign        = CryptoJS.HmacSHA256(base_string, partner_key).toString()
    const header    = {headers : {"Content-Type": "application/json"}}
    const api       = `${host}${ShopeeAPI.GET_ITEM_PROMOTION}?item_id_list=${item_id_list}&shop_id=${Number(shop_id)}&partner_id=${Number(partner_id)}&access_token=${access_token.token}&sign=${sign}&timestamp=${timestamp}`
    
    return await axios.get(api).then(async (result) => {
        return result.data
    })
}

export const get_sip_item_price_service = async (shop_id, data, user_id) => {
    const access_token = await AccessToken(user_id, shop_id)
    let base_string = `${partner_id}${ShopeeAPI.UPDATE_SIP_ITEM_PRICE}${Number(timestamp)}${access_token.token}${Number(shop_id)}`;
    let sign        = CryptoJS.HmacSHA256(base_string, partner_key).toString()
    const header    = {headers : {"Content-Type": "application/json"}}
    const api       = `${host}${ShopeeAPI.UPDATE_SIP_ITEM_PRICE}?shop_id=${Number(shop_id)}&partner_id=${Number(partner_id)}&access_token=${access_token.token}&sign=${sign}&timestamp=${timestamp}`
    
    return await axios.post(api, data. header).then(async (result) => {
        return result.data
    })
}

export const search_item_service = async (shop_id, page_size, item_name, user_id) => {
    const access_token = await AccessToken(user_id, shop_id)
    let base_string = `${partner_id}${ShopeeAPI.SEARCH_ITEM}${Number(timestamp)}${access_token.token}${Number(shop_id)}`;
    let sign        = CryptoJS.HmacSHA256(base_string, partner_key).toString()
    const header    = {headers : {"Content-Type": "application/json"}}
    const api       = `${host}${ShopeeAPI.SEARCH_ITEM}?page_size=${page_size}&item_name=${item_name}&shop_id=${Number(shop_id)}&partner_id=${Number(partner_id)}&access_token=${access_token.token}&sign=${sign}&timestamp=${timestamp}`
    
    return await axios.get(api).then(async (result) => {
        return result.data
    })
}

export const category_recommend_service = async (shop_id, item_name, user_id) => {
    const access_token = await AccessToken(user_id, shop_id)
    let base_string = `${partner_id}${ShopeeAPI.CATEGORY_RECOMMEND}${Number(timestamp)}${access_token.token}${Number(shop_id)}`;
    let sign        = CryptoJS.HmacSHA256(base_string, partner_key).toString()
    const header    = {headers : {"Content-Type": "application/json"}}
    const api       = `${host}${ShopeeAPI.CATEGORY_RECOMMEND}?item_name=${item_name}&shop_id=${Number(shop_id)}&partner_id=${Number(partner_id)}&access_token=${access_token.token}&sign=${sign}&timestamp=${timestamp}`
    
    return await axios.get(api).then(async (result) => {
        return result.data
    })
}

export const get_order_list_service = async (shop_id, time_range_field, time_from, time_to, page_size, user_id) => {
    const access_token = await AccessToken(user_id, shop_id)
    let base_string = `${partner_id}${ShopeeAPI.GET_ORDER_LIST}${Number(timestamp)}${access_token.token}${Number(shop_id)}`;
    let sign        = CryptoJS.HmacSHA256(base_string, partner_key).toString()
    const header    = {headers : {"Content-Type": "application/json"}}
    const params    = `time_range_field=${time_range_field}&time_from=${time_from}&time_to=${time_to}&page_size=${page_size}`
    const api       = `${host}${ShopeeAPI.GET_ORDER_LIST}?${params}&shop_id=${Number(shop_id)}&partner_id=${Number(partner_id)}&access_token=${access_token.token}&sign=${sign}&timestamp=${timestamp}`
    
    return await axios.get(api).then(async (result) => {
        return result.data
    })
}

export const get_shipment_list_service = async (shop_id, page_size, user_id) => {
    const access_token = await AccessToken(user_id, shop_id)
    let base_string = `${partner_id}${ShopeeAPI.GET_SHIPMENT_LIST}${Number(timestamp)}${access_token.token}${Number(shop_id)}`;
    let sign        = CryptoJS.HmacSHA256(base_string, partner_key).toString()
    const header    = {headers : {"Content-Type": "application/json"}}
    const params    = `page_size=${page_size}`
    const api       = `${host}${ShopeeAPI.GET_SHIPMENT_LIST}?${params}&shop_id=${Number(shop_id)}&partner_id=${Number(partner_id)}&access_token=${access_token.token}&sign=${sign}&timestamp=${timestamp}`
    
    return await axios.get(api).then(async (result) => {
        return result.data
    })
}

export const get_order_detail_service = async (shop_id, order_sn_list , user_id) => {
    const access_token = await AccessToken(user_id, shop_id)
    let base_string = `${partner_id}${ShopeeAPI.GET_ORDER_DETAIL}${Number(timestamp)}${access_token.token}${Number(shop_id)}`;
    let sign        = CryptoJS.HmacSHA256(base_string, partner_key).toString()
    const header    = {headers : {"Content-Type": "application/json"}}
    const params    = `order_sn_list=${order_sn_list }&response_optional_fields=`
    const api       = `${host}${ShopeeAPI.GET_ORDER_DETAIL}?${params}&shop_id=${Number(shop_id)}&partner_id=${Number(partner_id)}&access_token=${access_token.token}&sign=${sign}&timestamp=${timestamp}`
    
    return await axios.get(api).then(async (result) => {
        return result.data
    })
}

export const split_order_service = async (shop_id, data , user_id) => {
    const access_token = await AccessToken(user_id, shop_id)
    let base_string = `${partner_id}${ShopeeAPI.SPLIT_ORDER}${Number(timestamp)}${access_token.token}${Number(shop_id)}`;
    let sign        = CryptoJS.HmacSHA256(base_string, partner_key).toString()
    const header    = {headers : {"Content-Type": "application/json"}}
    const api       = `${host}${ShopeeAPI.SPLIT_ORDER}?shop_id=${Number(shop_id)}&partner_id=${Number(partner_id)}&access_token=${access_token.token}&sign=${sign}&timestamp=${timestamp}`
    
    return await axios.post(api, data, header).then(async (result) => {
        return result.data
    })
}

export const unsplit_order_service = async (shop_id, data , user_id) => {
    const access_token = await AccessToken(user_id, shop_id)
    let base_string = `${partner_id}${ShopeeAPI.UNSPLIT_ORDER}${Number(timestamp)}${access_token.token}${Number(shop_id)}`;
    let sign        = CryptoJS.HmacSHA256(base_string, partner_key).toString()
    const header    = {headers : {"Content-Type": "application/json"}}
    const api       = `${host}${ShopeeAPI.UNSPLIT_ORDER}?shop_id=${Number(shop_id)}&partner_id=${Number(partner_id)}&access_token=${access_token.token}&sign=${sign}&timestamp=${timestamp}`
    
    return await axios.post(api, data, header).then(async (result) => {
        return result.data
    })
}

export const cancel_order_service = async (shop_id, data , user_id) => {
    const access_token = await AccessToken(user_id, shop_id)
    let base_string = `${partner_id}${ShopeeAPI.CANCEL_ORDER}${Number(timestamp)}${access_token.token}${Number(shop_id)}`;
    let sign        = CryptoJS.HmacSHA256(base_string, partner_key).toString()
    const header    = {headers : {"Content-Type": "application/json"}}
    const api       = `${host}${ShopeeAPI.CANCEL_ORDER}?shop_id=${Number(shop_id)}&partner_id=${Number(partner_id)}&access_token=${access_token.token}&sign=${sign}&timestamp=${timestamp}`
    
    const body = {
        order_sn: data.order_sn,
        cancel_reason:data.cancel_reason,
        item_list:data.item_list
    }
    return await axios.post(api, body, header).then(async (result) => {
        return result.data
    })
}

export const handler_buyer_cancelation_service = async (shop_id, data , user_id) => {
    const access_token = await AccessToken(user_id, shop_id)
    let base_string = `${partner_id}${ShopeeAPI.HANDLER_BUYER_CANCELATION}${Number(timestamp)}${access_token.token}${Number(shop_id)}`;
    let sign        = CryptoJS.HmacSHA256(base_string, partner_key).toString()
    const header    = {headers : {"Content-Type": "application/json"}}
    const api       = `${host}${ShopeeAPI.HANDLER_BUYER_CANCELATION}?shop_id=${Number(shop_id)}&partner_id=${Number(partner_id)}&access_token=${access_token.token}&sign=${sign}&timestamp=${timestamp}`
    
    const body = {
        order_sn: data.order_sn,
        operation :data.operation ,
    }
    return await axios.post(api, body, header).then(async (result) => {
        return result.data
    })
}

export const add_invoice_data_service = async (shop_id, data , user_id) => {
    const access_token = await AccessToken(user_id, shop_id)
    let base_string = `${partner_id}${ShopeeAPI.ADD_INVOICE_DATA}${Number(timestamp)}${access_token.token}${Number(shop_id)}`;
    let sign        = CryptoJS.HmacSHA256(base_string, partner_key).toString()
    const header    = {headers : {"Content-Type": "application/json"}}
    const api       = `${host}${ShopeeAPI.ADD_INVOICE_DATA}?shop_id=${Number(shop_id)}&partner_id=${Number(partner_id)}&access_token=${access_token.token}&sign=${sign}&timestamp=${timestamp}`
    
    const body = {
        order_sn : data.order_sn ,
        invoice_data:data.invoice_data
    }
    return await axios.post(api, body, header).then(async (result) => {
        return result.data
    })
}

export const get_return_list_service = async (shop_id, page_no, page_size, create_time_from, create_time_to, user_id) => {
    const access_token = await AccessToken(user_id, shop_id)
    let base_string = `${partner_id}${ShopeeAPI.GET_RETURN_LIST}${Number(timestamp)}${access_token.token}${Number(shop_id)}`;
    let sign        = CryptoJS.HmacSHA256(base_string, partner_key).toString()
    const header    = {headers : {"Content-Type": "application/json"}}
    const params    = `page_no =${page_no}&page_size=${page_size }&create_time_from=${create_time_from}&create_time_to=${create_time_to}`
    const api       = `${host}${ShopeeAPI.GET_RETURN_LIST}?${params}&shop_id=${Number(shop_id)}&partner_id=${Number(partner_id)}&access_token=${access_token.token}&sign=${sign}&timestamp=${timestamp}`
    
    return await axios.get(api).then(async (result) => {
        return result.data
    })
}

export const get_return_detail_service = async (shop_id, return_sn , user_id) => {
    const access_token = await AccessToken(user_id, shop_id)
    let base_string = `${partner_id}${ShopeeAPI.GET_RETURN_DETAIL}${Number(timestamp)}${access_token.token}${Number(shop_id)}`;
    let sign        = CryptoJS.HmacSHA256(base_string, partner_key).toString()
    const header    = {headers : {"Content-Type": "application/json"}}
    const params    = `return_sn=${return_sn}`
    const api       = `${host}${ShopeeAPI.GET_RETURN_DETAIL}?${params}&shop_id=${Number(shop_id)}&partner_id=${Number(partner_id)}&access_token=${access_token.token}&sign=${sign}&timestamp=${timestamp}`
    
    return await axios.get(api).then(async (result) => {
        return result.data
    })
}

export const get_shipping_parameter_service = async (shop_id, order_sn  , user_id) => {
    const access_token = await AccessToken(user_id, shop_id)
    let base_string = `${partner_id}${ShopeeAPI.GET_SHIPPING_PARAMETER}${Number(timestamp)}${access_token.token}${Number(shop_id)}`;
    let sign        = CryptoJS.HmacSHA256(base_string, partner_key).toString()
    const header    = {headers : {"Content-Type": "application/json"}}
    const params    = `order_sn =${order_sn}`
    const api       = `${host}${ShopeeAPI.GET_SHIPPING_PARAMETER}?${params}&shop_id=${Number(shop_id)}&partner_id=${Number(partner_id)}&access_token=${access_token.token}&sign=${sign}&timestamp=${timestamp}`
    
    return await axios.get(api).then(async (result) => {
        return result.data
    })
}

export const get_tracking_number_service = async (shop_id, data, user_id) => {
    const access_token = await AccessToken(user_id, shop_id)
    let base_string = `${partner_id}${ShopeeAPI.GET_TRACKING_NUMBER}${Number(timestamp)}${access_token.token}${Number(shop_id)}`;
    let sign        = CryptoJS.HmacSHA256(base_string, partner_key).toString()
    const header    = {headers : {"Content-Type": "application/json"}}
    const params    = `order_sn=${data.order_sn}&package_number=${data.package_number}&response_optional_fields=${data.response_optional_fields}`
    const api       = `${host}${ShopeeAPI.GET_TRACKING_NUMBER}?${params}&shop_id=${Number(shop_id)}&partner_id=${Number(partner_id)}&access_token=${access_token.token}&sign=${sign}&timestamp=${timestamp}`
    
    return await axios.get(api).then(async (result) => {
        return result.data
    })
}

export const get_ship_order_service = async (shop_id, data, user_id) => {
    const access_token = await AccessToken(user_id, shop_id)
    let base_string = `${partner_id}${ShopeeAPI.SHIP_ORDER}${Number(timestamp)}${access_token.token}${Number(shop_id)}`;
    let sign        = CryptoJS.HmacSHA256(base_string, partner_key).toString()
    const header    = {headers : {"Content-Type": "application/json"}}
    const api       = `${host}${ShopeeAPI.SHIP_ORDER}?shop_id=${Number(shop_id)}&partner_id=${Number(partner_id)}&access_token=${access_token.token}&sign=${sign}&timestamp=${timestamp}`
    
    return await axios.post(api, data, header).then(async (result) => {
        return result.data
    })
}

export const update_shipping_order_service = async (shop_id, data, user_id) => {
    const access_token = await AccessToken(user_id, shop_id)
    let base_string = `${partner_id}${ShopeeAPI.UPDATE_SHIPPING_ORDER}${Number(timestamp)}${access_token.token}${Number(shop_id)}`;
    let sign        = CryptoJS.HmacSHA256(base_string, partner_key).toString()
    const header    = {headers : {"Content-Type": "application/json"}}
    const api       = `${host}${ShopeeAPI.UPDATE_SHIPPING_ORDER}?shop_id=${Number(shop_id)}&partner_id=${Number(partner_id)}&access_token=${access_token.token}&sign=${sign}&timestamp=${timestamp}`
    
    return await axios.post(api, data, header).then(async (result) => {
        return result.data
    })
}

export const get_shipping_document_parameter_service = async (shop_id, data, user_id) => {
    const access_token = await AccessToken(user_id, shop_id)
    let base_string = `${partner_id}${ShopeeAPI.GET_SHIPPING_DOCUMENT_PARAMETER}${Number(timestamp)}${access_token.token}${Number(shop_id)}`;
    let sign        = CryptoJS.HmacSHA256(base_string, partner_key).toString()
    const header    = {headers : {"Content-Type": "application/json"}}
    const api       = `${host}${ShopeeAPI.GET_SHIPPING_DOCUMENT_PARAMETER}?shop_id=${Number(shop_id)}&partner_id=${Number(partner_id)}&access_token=${access_token.token}&sign=${sign}&timestamp=${timestamp}`
    
    return await axios.post(api, data, header).then(async (result) => {
        return result.data
    })
}

export const create_shipping_document_service = async (shop_id, data, user_id) => {
    const access_token = await AccessToken(user_id, shop_id)
    let base_string = `${partner_id}${ShopeeAPI.CREATE_SHIPPING_DOCUMENT}${Number(timestamp)}${access_token.token}${Number(shop_id)}`;
    let sign        = CryptoJS.HmacSHA256(base_string, partner_key).toString()
    const header    = {headers : {"Content-Type": "application/json"}}
    const api       = `${host}${ShopeeAPI.CREATE_SHIPPING_DOCUMENT}?shop_id=${Number(shop_id)}&partner_id=${Number(partner_id)}&access_token=${access_token.token}&sign=${sign}&timestamp=${timestamp}`
    
    return await axios.post(api, data, header).then(async (result) => {
        return result.data
    })
}

export const get_shipping_document_result_service = async (shop_id, data, user_id) => {
    const access_token = await AccessToken(user_id, shop_id)
    let base_string = `${partner_id}${ShopeeAPI.GET_SHIPPING_DOCUMENT_RESULT}${Number(timestamp)}${access_token.token}${Number(shop_id)}`;
    let sign        = CryptoJS.HmacSHA256(base_string, partner_key).toString()
    const header    = {headers : {"Content-Type": "application/json"}}
    const api       = `${host}${ShopeeAPI.GET_SHIPPING_DOCUMENT_RESULT}?shop_id=${Number(shop_id)}&partner_id=${Number(partner_id)}&access_token=${access_token.token}&sign=${sign}&timestamp=${timestamp}`
    
    return await axios.post(api, data, header).then(async (result) => {
        return result.data
    })
}

export const get_tracking_info_service = async (shop_id, order_sn, package_number, user_id) => {
    const access_token = await AccessToken(user_id, shop_id)
    let base_string = `${partner_id}${ShopeeAPI.GET_TRACKING_INFO}${Number(timestamp)}${access_token.token}${Number(shop_id)}`;
    let sign        = CryptoJS.HmacSHA256(base_string, partner_key).toString()
    const header    = {headers : {"Content-Type": "application/json"}}
    const params    = `order_sn=${order_sn}&package_number=${package_number}`
    const api       = `${host}${ShopeeAPI.GET_TRACKING_INFO}?${params}&shop_id=${Number(shop_id)}&partner_id=${Number(partner_id)}&access_token=${access_token.token}&sign=${sign}&timestamp=${timestamp}`
    
    return await axios.get(api).then(async (result) => {
        return result.data
    })
}

export const get_address_list_service = async (shop_id, user_id) => {
    const access_token = await AccessToken(user_id, shop_id)
    let base_string = `${partner_id}${ShopeeAPI.GET_ADDRESS_LIST}${Number(timestamp)}${access_token.token}${Number(shop_id)}`;
    let sign        = CryptoJS.HmacSHA256(base_string, partner_key).toString()
    const header    = {headers : {"Content-Type": "application/json"}}
    const api       = `${host}${ShopeeAPI.GET_ADDRESS_LIST}?shop_id=${Number(shop_id)}&partner_id=${Number(partner_id)}&access_token=${access_token.token}&sign=${sign}&timestamp=${timestamp}`
    
    return await axios.get(api).then(async (result) => {
        return result.data
    })
}

export const get_channel_list_service = async (shop_id, user_id) => {
    const access_token = await AccessToken(user_id, shop_id)
    let base_string = `${partner_id}${ShopeeAPI.GET_CHANNEL_LIST}${Number(timestamp)}${access_token.token}${Number(shop_id)}`;
    let sign        = CryptoJS.HmacSHA256(base_string, partner_key).toString()
    const header    = {headers : {"Content-Type": "application/json"}}
    const api       = `${host}${ShopeeAPI.GET_CHANNEL_LIST}?shop_id=${Number(shop_id)}&partner_id=${Number(partner_id)}&access_token=${access_token.token}&sign=${sign}&timestamp=${timestamp}`
    
    return await axios.get(api).then(async (result) => {
        return result.data
    })
}

export const update_channel_service = async (shop_id, data, user_id) => {
    const access_token = await AccessToken(user_id, shop_id)
    let base_string = `${partner_id}${ShopeeAPI.UPDATE_CHANNEL}${Number(timestamp)}${access_token.token}${Number(shop_id)}`;
    let sign        = CryptoJS.HmacSHA256(base_string, partner_key).toString()
    const header    = {headers : {"Content-Type": "application/json"}}
    const api       = `${host}${ShopeeAPI.UPDATE_CHANNEL}?shop_id=${Number(shop_id)}&partner_id=${Number(partner_id)}&access_token=${access_token.token}&sign=${sign}&timestamp=${timestamp}`
    
    return await axios.post(api, data, header).then(async (result) => {
        return result.data
    })
}

export const batch_ship_order_service = async (shop_id, data, user_id) => {
    const access_token = await AccessToken(user_id, shop_id)
    let base_string = `${partner_id}${ShopeeAPI.BATCH_SHIP_ORDER}${Number(timestamp)}${access_token.token}${Number(shop_id)}`;
    let sign        = CryptoJS.HmacSHA256(base_string, partner_key).toString()
    const header    = {headers : {"Content-Type": "application/json"}}
    const api       = `${host}${ShopeeAPI.BATCH_SHIP_ORDER}?shop_id=${Number(shop_id)}&partner_id=${Number(partner_id)}&access_token=${access_token.token}&sign=${sign}&timestamp=${timestamp}`
    
    return await axios.post(api, data, header).then(async (result) => {
        return result.data
    })
}

export const get_unbind_order_list_service = async (shop_id, data, user_id) => {
    const access_token = await AccessToken(user_id, shop_id)
    let base_string = `${partner_id}${ShopeeAPI.GET_UNBIND_ORDER_LIST}${Number(timestamp)}${access_token.token}${Number(shop_id)}`;
    let sign        = CryptoJS.HmacSHA256(base_string, partner_key).toString()
    const header    = {headers : {"Content-Type": "application/json"}}
    const params    = `cursor=${data.cursor}&page_size=${data.page_size}&response_optional_fields=${data.response_optional_fields}`
    const api       = `${host}${ShopeeAPI.GET_UNBIND_ORDER_LIST}?${params}&shop_id=${Number(shop_id)}&partner_id=${Number(partner_id)}&access_token=${access_token.token}&sign=${sign}&timestamp=${timestamp}`
    
    return await axios.get(api).then(async (result) => {
        return result.data
    })
}

export const get_detail_delivery_service = async (shop_id, data, user_id) => {
    const access_token = await AccessToken(user_id, shop_id)
    let base_string = `${partner_id}${ShopeeAPI.GET_DETAIL_DELIVERY}${Number(timestamp)}${access_token.token}${Number(shop_id)}`;
    let sign        = CryptoJS.HmacSHA256(base_string, partner_key).toString()
    const header    = {headers : {"Content-Type": "application/json"}}
    const params    = `cursor=${data.cursor}&first_mile_tracking_number=${data.first_mile_tracking_number }`
    const api       = `${host}${ShopeeAPI.GET_DETAIL_DELIVERY}?${params}&shop_id=${Number(shop_id)}&partner_id=${Number(partner_id)}&access_token=${access_token.token}&sign=${sign}&timestamp=${timestamp}`
    
    return await axios.get(api).then(async (result) => {
        return result.data
    })
}

export const generate_first_mile_tracking_number_service = async (shop_id, data, user_id) => {
    const access_token = await AccessToken(user_id, shop_id)
    let base_string = `${partner_id}${ShopeeAPI.GENERATE_FIRST_MILE_TRACKING_NUMBER}${Number(timestamp)}${access_token.token}${Number(shop_id)}`;
    let sign        = CryptoJS.HmacSHA256(base_string, partner_key).toString()
    const header    = {headers : {"Content-Type": "application/json"}}
    const api       = `${host}${ShopeeAPI.GENERATE_FIRST_MILE_TRACKING_NUMBER}?shop_id=${Number(shop_id)}&partner_id=${Number(partner_id)}&access_token=${access_token.token}&sign=${sign}&timestamp=${timestamp}`
    
    return await axios.post(api, data, header).then(async (result) => {
        return result.data
    })
}

export const bind_first_mile_tracking_number_service = async (shop_id, data, user_id) => {
    const access_token = await AccessToken(user_id, shop_id)
    let base_string = `${partner_id}${ShopeeAPI.BIND_FIRST_MILE_TRACKING_NUMBER}${Number(timestamp)}${access_token.token}${Number(shop_id)}`;
    let sign        = CryptoJS.HmacSHA256(base_string, partner_key).toString()
    const header    = {headers : {"Content-Type": "application/json"}}
    const api       = `${host}${ShopeeAPI.BIND_FIRST_MILE_TRACKING_NUMBER}?shop_id=${Number(shop_id)}&partner_id=${Number(partner_id)}&access_token=${access_token.token}&sign=${sign}&timestamp=${timestamp}`
    
    return await axios.post(api, data, header).then(async (result) => {
        return result.data
    })
}

export const unbind_first_mile_tracking_number_service = async (shop_id, data, user_id) => {
    const access_token = await AccessToken(user_id, shop_id)
    let base_string = `${partner_id}${ShopeeAPI.UNBIND_FIRST_MILE_TRACKING_NUMBER}${Number(timestamp)}${access_token.token}${Number(shop_id)}`;
    let sign        = CryptoJS.HmacSHA256(base_string, partner_key).toString()
    const header    = {headers : {"Content-Type": "application/json"}}
    const api       = `${host}${ShopeeAPI.UNBIND_FIRST_MILE_TRACKING_NUMBER}?shop_id=${Number(shop_id)}&partner_id=${Number(partner_id)}&access_token=${access_token.token}&sign=${sign}&timestamp=${timestamp}`
    
    return await axios.post(api, data, header).then(async (result) => {
        return result.data
    })
}

export const get_tracking_number_list_service = async (shop_id, data, user_id) => {
    const access_token = await AccessToken(user_id, shop_id)
    let base_string = `${partner_id}${ShopeeAPI.GET_TRACKING_NUMBER_LIST}${Number(timestamp)}${access_token.token}${Number(shop_id)}`;
    let sign        = CryptoJS.HmacSHA256(base_string, partner_key).toString()
    const header    = {headers : {"Content-Type": "application/json"}}
    const params    = `from_date=${data.from_date}&to_date=${data.to_date}&page_size=${data.page_size}&cursor=${data.cursor}`
    const api       = `${host}${ShopeeAPI.GET_TRACKING_NUMBER_LIST}?${params}&shop_id=${Number(shop_id)}&partner_id=${Number(partner_id)}&access_token=${access_token.token}&sign=${sign}&timestamp=${timestamp}`
    
    return await axios.get(api).then(async (result) => {
        return result.data
    })
}

export const get_waybill_service = async (shop_id, data, user_id) => {
    const access_token = await AccessToken(user_id, shop_id)
    let base_string = `${partner_id}${ShopeeAPI.GET_WAYBILL}${Number(timestamp)}${access_token.token}${Number(shop_id)}`;
    let sign        = CryptoJS.HmacSHA256(base_string, partner_key).toString()
    const header    = {headers : {"Content-Type": "application/json"}}
    const api       = `${host}${ShopeeAPI.GET_WAYBILL}?shop_id=${Number(shop_id)}&partner_id=${Number(partner_id)}&access_token=${access_token.token}&sign=${sign}&timestamp=${timestamp}`
    
    return await axios.post(api, data, header).then(async (result) => {
        return result.data
    })
}

export const get_channel_list_order_service = async (shop_id, data, user_id) => {
    const access_token = await AccessToken(user_id, shop_id)
    let base_string = `${partner_id}${ShopeeAPI.GET_CHANNEL_ORDER_LIST}${Number(timestamp)}${access_token.token}${Number(shop_id)}`;
    let sign        = CryptoJS.HmacSHA256(base_string, partner_key).toString()
    const header    = {headers : {"Content-Type": "application/json"}}
    const params    = `region=${data.region}`
    const api       = `${host}${ShopeeAPI.GET_CHANNEL_ORDER_LIST}?${params}&shop_id=${Number(shop_id)}&partner_id=${Number(partner_id)}&access_token=${access_token.token}&sign=${sign}&timestamp=${timestamp}`
    
    return await axios.get(api).then(async (result) => {
        return result.data
    })
}

