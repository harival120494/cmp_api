import Role from "../models/RoleModel.js";
import OTP from "../helper/OTPGenerator.js";
import moment from 'moment';

/**
 * ROLE SERVICE
 * ==============================
 * Created by Harival Tivani
 * Created at December 29, 2021
 * ==============================
 */

export const user_role_service = async () => {
    try {
        return ({status: 200, data :await Role.findAll({},{type : Role.sequelize.QueryTypes.SELECT})});
    } catch (error) {
        return error;
    }
}

export const user_role_byid_service = async (id) => {
    try {
        return({status : 200, data : await Role.findOne({where : {id_role : id}},{type : Role.sequelize.QueryTypes.SELECT})});
    } catch (error) {
        return error;
    }
}

export const create_user_role_service = async (req, user_id) => {
    try {
        return await Role.create(
            { 
                role_name : req.role_name,
                kode_role: `ROL${OTP}`, 
                desc: req.desc,
                status: req.status,
                created_by : user_id
            }
        )
        .then((result) => {
            return({status : 200, data : result});
        })

    } catch (error) {
        return error;
    }
}

export const update_user_role_service = async (req, id_role, user_id) => {
    try {
        return await Role.update(
            { 
                role_name : req.role_name,
                desc: req.desc,
                status: req.status,
                updated_by : user_id,
                updated_at : moment().format('YYYY-MM-DD HH:mm:ss')
            },
            {
                where: {
                    id_role: id_role
                },
                type : Role.sequelize.QueryTypes.UPDATE
            }
        )
        .then((result) => {
            if(result == 1){return ({status : 200, data : 'Data updated successfully'})}
            else{return ({status : 400, data : 'Data update failed'})}
        })

    } catch (error) {
        return error;
    }
}

export const delete_user_role_service = async (id_role) => {
    try {
        return await Role.destroy(
            {
                where: {
                    id_role: id_role
                },
                type : Role.sequelize.QueryTypes.DELETE
            }
        )
        .then((result) => {
            if(result == 1){return ({status : 200, data : 'Delete data successfully'})}
            else{return ({status : 400, data : 'Delete data failed'})}
        })
    } catch (error) {
        return error;
    }
}

export const role_cs_service = async (id_role, status) => {
    try {
        return Role.update({ 
            status:status,
            updated_at : moment().format('YYYY-MM-DD HH:mm:ss')
        }, 
        {
            where: {
                id_role: id_role
            },
            type : Role.sequelize.QueryTypes.UPDATE
        })
        .then(result => {
            if(result == 1){return ({status : 200, message : 'Status berhasil dirubah'})}
            else{return ({status : 400, message : 'Status gagal dirubah'})}
        })
    } catch (error) {
        return error;
    }
}