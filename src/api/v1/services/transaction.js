import DShop from '../models/DShopModel.js'
import Package from '../models/PackageModel.js'
import { Sequelize } from 'sequelize';
import ShopeeAPI from "../../../../const/Shopee.js";
import CryptoJS from "crypto-js";
import axios from 'axios';
import {AccessToken} from '../helper/AccessToken.js'
import dotenv from "dotenv";
dotenv.config();

/**
 * TRANSACTION SERVICE
 * ==============================
 * Created by Harival Tivani
 * Created at January 25, 2021
 * ==============================
 */

const timestamp = Math.floor(new Date().valueOf() / 1000)
const partner_id = Number(ShopeeAPI.SHOPEE_PARTNER_ID)
const partner_key = ShopeeAPI.SHOPEE_PARTNER_KEY
const host = ShopeeAPI.SHOPEE_AUTH_HOST

export const list_toko_service = async (user_id) => {
    try {
        const query = `SELECT
                            ds.shop_id,
                            ds.shop_id_marketplace,
                            ds.shop_name,
                            ds.shop_logo,
                            cm.marketplace_name,
                            CASE WHEN (ds.status = 1)
                                THEN 'Terhubung'
                                ELSE 'Belum Terhubung'
                            end as stat,
                            ds.created_date
                        FROM d_shop as ds
                        LEFT JOIN c_marketplace as cm ON ds.marketplace_id = cm.marketplace_id
                        WHERE ds.user_id = $1`;
        return await DShop.sequelize.query(query, {
                            bind : [user_id],
                            type: Sequelize.QueryTypes.SELECT
                        })
                        .then(async (result) => {
                            return ({status : 200, data : result})
                        })
        
    } catch (error) {
        return error;
    }
}

export const list_paket_user_service = async (user_id) => {
    try {
        const query = `SELECT
                            top.order_package_id,
                            top.no_invoice,
                            top.jumlah_toko,
                            top.harga_pertoko,
                            top.created_date,
                            cb.bank_name,
                            cas.status_name
                        FROM tr_order_package as top
                        LEFT JOIN c_bank as cb ON cb.bank_id = top.bank_id 
                        LEFT JOIN c_all_status as cas ON cas.status_id = top.status_id
                        WHERE top.user_id = $1`;
        return await Package.sequelize.query(query, {
                            bind : [user_id],
                            type: Sequelize.QueryTypes.SELECT
                        })
                        .then(async (result) => {
                            return ({status : 200, data : result})
                        })
        
    } catch (error) {
        return error;
    }
}

export const detail_package_order_service = async (package_order_id, user_id, BASE_URL) => {
    const query_header = `SELECT
                            top.no_invoice,
                            top.jumlah_toko,
                            top.harga_pertoko,
                            cas.status_id,
                            cas.status_name
                        FROM tr_order_package as top
                        LEFT JOIN c_all_status as cas ON cas.status_id = top.status_id
                        WHERE top.user_id = $1 AND top.order_package_id = $2`;

    const query_detail = `SELECT
                            top.order_package_id,
                            top.approve_date as tgl_aktif,
                            tdop.harga_satuan,
                            cm.marketplace_name,
                            CONCAT('${BASE_URL}',cm.logo_url) logo_url,
                            dp.package_name,
                            cp.jumlah_hari,
                            DATE_ADD(top.approve_date, INTERVAL cp.jumlah_hari DAY) as end_date
                        FROM tr_order_package as top
                        LEFT JOIN tr_detail_order_package as tdop ON tdop.order_package_id = top.order_package_id
                        LEFT JOIN d_package as dp ON dp.package_id = tdop.package_id
                        LEFT JOIN c_marketplace AS cm ON cm.marketplace_id = dp.marketplace_id
                        LEFT JOIN c_periode as cp ON cp.periode_id = dp.periode_id
                        WHERE top.order_package_id = $1`

    const data_header = await Package.sequelize.query(query_header, {
                    bind : [user_id, package_order_id],
                    type: Sequelize.QueryTypes.SELECT
                }).then(async (result) => {
                    return result[0]
                })
    const data_detail = await Package.sequelize.query(query_detail, {
                    bind : [package_order_id],
                    type : Sequelize.QueryTypes.SELECT
                }).then(async (result) => {
                    return result
                })

    return {status : 200, data_header : data_header, data_detail : data_detail}
}

export const data_invoice_service = async (package_order_id, user_id) => {
    const query_header = `SELECT
                                top.no_invoice,
                                users.pic_name,
                                users.email,
                                top.tgl_pembayaran
                            FROM tr_order_package as top
                            LEFT JOIN users ON users.user_id = top.user_id
                            WHERE top.order_package_id = $1`;
    
    const query_detail = `SELECT
                                dp.package_name,
                                tdop.harga_satuan,
                                tdop.qty,
                                (tdop.harga_satuan * tdop.qty)  as total_harga
                            FROM tr_order_package as top
                            LEFT JOIN tr_detail_order_package as tdop ON tdop.order_package_id = top.order_package_id
                            LEFT JOIN d_package as dp ON dp.package_id = tdop.package_id
                            WHERE top.order_package_id = $1`;

    const query_footer = `SELECT
                            'Toko' as nama_toko,
                            top.jumlah_toko,
                            top.harga_pertoko,
                            cb.bank_name
                        FROM tr_order_package as top
                        LEFT JOIN c_bank as cb ON cb.bank_id = top.bank_id
                        WHERE top.order_package_id = $1`;
    
    const data_header = await Package.sequelize.query(query_header, {
                    bind : [package_order_id],
                    type: Sequelize.QueryTypes.SELECT
                }).then(async (result) => {
                    return result[0]
                })
    const data_detail = await Package.sequelize.query(query_detail, {
                    bind : [package_order_id],
                    type : Sequelize.QueryTypes.SELECT
                }).then(async (result) => {
                    return result
                })
    const data_footer = await Package.sequelize.query(query_footer, {
                    bind : [package_order_id],
                    type : Sequelize.QueryTypes.SELECT
                }).then(async (result) => {
                    return result
                })

    return {status : 200, data_header : data_header, data_detail : data_detail, data_footer:data_footer}
}

export const update_profile_toko_service = async (data, shop_logo, shop_id, user_id) => {
    const data_body = { 
        shop_name : shop_name,
        shop_logo : 'https://d1nhio0ox7pgb.cloudfront.net/_img/g_collection_png/standard/256x256/store.png',
        description : data.description
    }
    return await DShop.update(
        data_body,
        {
            where: { shop_id_marketplace : shop_id },
            type : DShop.sequelize.QueryTypes.update
        })
        .then(async (result) => {
            if(result[0] > 0){
                const access_token = await AccessToken(user_id, shop_id)
                let base_string = `${partner_id}${ShopeeAPI.UPDATE_PROFILE_SHOP}${Number(timestamp)}${access_token.token}${Number(shop_id)}`;
                let sign        = CryptoJS.HmacSHA256(base_string, partner_key).toString()
                const header    = {headers : {"Content-Type": "application/json"}}
                const api       = `${host}${ShopeeAPI.UPDATE_PROFILE_SHOP}?shop_id=${Number(shop_id)}&partner_id=${partner_id}&access_token=${access_token.token}&sign=${sign}&timestamp=${timestamp}`
                
                return await axios.post(api, data_body, header).then( (result) => {
                    console.log(result.data)
                })
            }
        })
}

